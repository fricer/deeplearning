\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2018

% ready for submission
% \usepackage{neurips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
     \usepackage[final]{style}

% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{neurips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{graphicx}

\renewcommand{\thesubsubsection}{\thesubsection.\alph{subsubsection}}

\title{Deep Learning\\Assignment1: MLPs, CNNs and Backpropagation}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{Ivan Yovchev\\
	12871737\\
	yovchev23@gmail.com
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

\section{MLP backprop and NumPy implementation}

\subsection{Analytical derivation of gradients}

\subsubsection{}

First we derive the gradient of the cross entropy w.r.t to $x^{(N)}$:
\begin{equation}
	\begin{aligned}
		\frac{\partial L}{\partial x_i^{(N)}} &= \frac{\partial \left(-\sum_{i}t_i logx_i^{(N)}\right)}{\partial x_i^{(N)}} = -\frac{t_i}{x_i^{(N)}} \\
%		&\Rightarrow \\
%		\frac{\partial L}{\partial x^{(N)}} &= -\frac{t}{x^{(N)}}
	\end{aligned}
\end{equation}

Next we compute the derivative at the softmax layer, which is split in two cases the first one being $i = j$:

\begin{equation}
	\begin{aligned}
		\frac{\partial x_i^{(N)}}{\partial \tilde{x}_j^{(N)}} &= \frac{\exp(\tilde{x}_i^{(N)})}{\sum_{n}\exp(\tilde{x}_n^{(N)})} - \frac{\exp(\tilde{x}_i^{(N)})\exp(\tilde{x}_j^{(N)})}{\left(\sum_{n}\exp(\tilde{x}_n^{(N)})\right)^2} = \\ &= x_i^{(N)} - x_i^{(N)}x_j^{(N)} = x_i^{(N)}(1 - x_j^{(N)})
	\end{aligned}
	\label{eq:2}
\end{equation}

Then we have the second case where $i \neq j$:

\begin{equation}
	\begin{aligned}
		\frac{\partial x_i^{(N)}}{\partial \tilde{x}_j^{(N)}} &= - \frac{\exp(\tilde{x}_i^{(N)})\exp(\tilde{x}_j^{(N)})}{\left(\sum_{n}\exp(\tilde{x}_n^{(N)})\right)^2} = -x_i^{(N)}x_j^{(N)}
	\end{aligned}
	\label{eq:3}
\end{equation}

Equations \eqref{eq:2} and \eqref{eq:3} can be combined using the $\mathbb{I}\{i = j\}$ operator which is a matrix comprised entirely of zeros except at $i = j$ where the value is 1:

\begin{equation}
	\frac{\partial x_i^{(N)}}{\partial \tilde{x}_j^{(N)}} = x_i^{(N)}(\mathbb{I}\{i = j\} - x_j^{(N)})
\end{equation}

Next we need to compute the derivative of the LeakyReLU for all layers $l < N$, where the LeakyReLU is defined as follows:

\begin{equation}
	LeakyReLU(x) = \begin{cases}
	x & \text{if $x > 0$}\\
	ax & \text{otherwise}
	\end{cases}
\end{equation}

which means the derivative is then the following:

\begin{equation}
	\begin{aligned}
		\frac{\partial x^{(l<N)}}{\partial \tilde{x}^{(l<N)}} = \frac{\partial LeakyReLU(\tilde{x}^{(l<N)})}{\partial \tilde{x}^{(l<N)}} = \begin{cases}
		1 & \text{if $x_i^{(l<N)} > 0$}\\
		a & \text{otherwise}
		\end{cases}
	\end{aligned}
\end{equation}

Next we compute the derivative of $\hat{x}$ w.r.t $x^{(l-1)}$:

\begin{equation}
	\begin{aligned}
		\frac{\partial \hat{x}}{\partial x^{(l-1)}} = \frac{\partial}{\partial x^{(l-1)}} W^{(l)}x^{(l-1)} + b^{(l)} = W^{(l)}
	\end{aligned}
\end{equation}

Similarly we also compute $\hat{x}$ w.r.t $b^{(l)}$:
\begin{equation}
	\begin{aligned}
		\frac{\partial \hat{x}}{\partial b^{(l)}} = \frac{\partial}{\partial b^{(l)}} W^{(l)}x^{(l-1)} + b^{(l)} = \mathbf{1}
	\end{aligned}
\end{equation}

Finally, we compute the derivative of $\hat{x}$ w.r.t $W^{(l)}$. To do so first we have to see that the derivative of a single output $\hat{x}_k$ with respect to one weight $W_{ij}$ is simply $x_j$ only if $k = i$ and it is zero in all other cases. This means that the derivative of a single output $\hat{x}_k$ w.r.t. to the entire matrix $W$ would result in a matrix where all the values are zeros expect for the row where $k = i$. That row has the value of $x^{(l - 1)}$ transposed:

\begin{equation}
	\begin{aligned}
		\frac{\partial \hat{x}_k}{\partial W} = \begin{bmatrix}
		\mathbf{0}^T \\
		\dots \\
		\mathbf{x^{(l - 1)}}^T \\
		\dots \\
		\mathbf{0}^T \\
		\end{bmatrix}
	\end{aligned}
	\label{eq:9}
\end{equation}

Finally then the derivative of $\hat{x}$ w.r.t $W^{(l)}$ would result in a tensor, where each element is a matrix of the form shown in \eqref{eq:9}, where a different row would correspond to $x^{(l-1)}$ given a different $k$:

\begin{equation}
	\frac{\partial \hat{x}}{\partial W} = \begin{bmatrix}
	\frac{\partial \hat{x}_1}{\partial W}\\
	\dots \\
	\frac{\partial \hat{x}_d}{\partial W}\\
	\end{bmatrix} = \begin{bmatrix}
		\begin{bmatrix}
			\mathbf{x^{(l - 1)}}^T \\
			\dots \\
			\mathbf{0}^T \\
		\end{bmatrix} \\
		\dots \\
		\dots \\
		\begin{bmatrix}
			\mathbf{0}^T \\
			\dots \\
			\mathbf{x^{(l - 1)}}^T\\
		\end{bmatrix} \\
	\end{bmatrix}
	\label{eq:10}
\end{equation}

\subsubsection{}

First we compute the following:
\begin{equation}
	\begin{aligned}
		\frac{\partial L}{\partial \hat{x}_j^{(N)}} = \sum_{i}\frac{\partial L}{\partial x_i^{(N)}}\frac{\partial x_i^{(N)}}{\partial \hat{x}_j^{(N)}} =\sum_{i} -\frac{t_i}{x_i^{(N)}}x_i^{(N)}(\mathbb{I}\{i = j\} - x_j^{(N)})
	\end{aligned}
\end{equation}

Next we have:
\begin{equation}
	\begin{aligned}
		 \frac{\partial L}{\partial \hat{x}_j^{(l<N)}} = \sum_{i}\frac{\partial L}{\partial x_i^{(l)}}\frac{\partial x_i^{(l)}}{\hat{x}_j^{(l)}} = \begin{cases}
			 \sum_{i}\frac{\partial L}{\partial x_i^{(l)}} & \text{if $x_i^{(l<N)} > 0$}\\
			 \sum_{i}\frac{\partial L}{\partial x_i^{(l)}}a & \text{otherwise}
		 \end{cases} 
	\end{aligned}
\end{equation}

Next we have the following:
\begin{equation}
	\begin{aligned}
		\frac{\partial L}{\partial x^{(l<N)}} = \frac{\partial L}{\partial \hat{x}^{(l + 1)}}\frac{\partial \hat{x}^{(l + 1)}}{\partial x^{(l)}} = \frac{\partial L}{\partial \hat{x}^{(l + 1)}}W^{(l + 1)}
	\end{aligned}
\end{equation}

Next we have:
\begin{equation}
	\begin{aligned}
		\frac{\partial L}{\partial b^{(l)}} = \frac{\partial L}{\partial \hat{x}^{(l)}}\frac{\partial \hat{x}^{(l)}}{\partial b^{(l)}} = \frac{\partial L}{\partial \hat{x}^{(l + 1)}}
	\end{aligned}
\end{equation}

From \eqref{eq:10} we saw that the result is a tensor and here a $\frac{\partial L}{\partial \hat{x}^{(l)}}$ is a vector meaning the result of the vector tensor multiplication would be a matrix of the form:

\begin{equation}
	\begin{aligned}
		\frac{\partial L}{\partial \hat{x}^{(l)}}\frac{\partial \hat{x}^{(l)}}{\partial W} = \begin{bmatrix}
			L_1\mathbf{x^{(l-1)}}^T\\
			\dots \\
			L_d\mathbf{x^{(l-1)}}^T\\
		\end{bmatrix}
	\end{aligned}
\end{equation} 

which can simply be broken down to the outer product between the two resulting vectors so we have:
\begin{equation}
	\frac{\partial L}{\partial \hat{x}^{(l)}}\frac{\partial \hat{x}^{(l)}}{\partial W} = \frac{\partial L}{\partial \hat{x}^{(l)}}\mathbf{x^{(l-1)}}
\end{equation}

\subsubsection{}

The result of having a batch size of $B \neq 1$ would be that each of the previously mentioned derivatives would have an extra dimension $B$ corresponding to the gradients and biases per data point contained in the batch.

\subsection{NumPy implementation}

The results on the test set after training an MLP implemented using Numpy can be found in \textit{Figure \ref{fig:mlp_numpy}}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/mlp_numpy.png}
	\caption{Loss and Accuracy of Numpy MLP on test set}
	\label{fig:mlp_numpy}
\end{figure}

\section{PyTorch MLP}

The results on the test set after training an MLP implemented using Pytorch can be found in \textit{Figure \ref{fig:mlp_numpy}}. The results were obtained by using an Adam optimizer and a parameter sweep was performed. The resulting accuracy was a achieved by using 4 hidden layers the first one with 1000 neurons, the second one with 500, the third one with 250 and the forth one with 125. The learning rate was set to $\alpha  = 0.0001$. The results can be found in \textit{Figure \ref{fig:mlp_pytorch}}. There were some parameters which where left at their default values, for example the batch size and maximum number of steps as it was too computationally expensive to do a full grid search. But theoretically even further improvements are possible by tuning the remaining hyper parameters.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/mlp_pytorch.png}
	\caption{Loss and Accuracy of MPL Pytorch on test set}
	\label{fig:mlp_pytorch}
\end{figure}

\section{Custom Module: Batch Normalization}

First we compute the derivative of the cross entropy w.r.t to gamma:

\begin{equation}
	\begin{aligned}
		\left(\frac{\partial L}{\partial \gamma}\right)_j = \sum_{s}\sum_{i}\frac{\partial L}{\partial y^s_i}\frac{\partial y^s_i}{\partial \gamma_j} = \sum_{s}\sum_{i}\frac{\partial L}{\partial y^s_i}\mathbb{I}\{i = j\}\hat{x}^s_i =  \sum_{s}\frac{\partial L}{\partial y^s_i}\hat{x}^s_i
	\end{aligned}
\end{equation}

Next we compute the derivative of the cross entropy w.r.t to beta:

\begin{equation}
	\begin{aligned}
		\left(\frac{\partial L}{\partial \beta}\right)_j = \sum_{s}\sum_{i}\frac{\partial L}{\partial y^s_i}\frac{\partial y^s_i}{\partial \beta_j} = \sum_{s}\sum_{i}\frac{\partial L}{\partial y^s_i}\mathbb{I}\{i = j\} =  \sum_{s}\frac{\partial L}{\partial y^s_i}
	\end{aligned}
\end{equation}

Next we compute the (much more complex) derivative of the cross entropy w.r.t to the input $x$:

\begin{equation}
	\begin{aligned}
		\left(\frac{\partial L}{\partial x}\right)^r_j &= \sum_{s}\sum_{i}\frac{\partial L}{\partial y^r_i}\frac{\partial y^s_i}{\partial x^r_j} = \sum_{s}\sum_{i}\frac{\partial L}{\partial y^r_i}\frac{\partial y^s_i}{\partial \hat{x}^r_j}\frac{\partial \hat{x}^s_i}{\partial x^r_j} = \sum_{s}\sum_{i}\gamma_i\frac{\partial L}{\partial y^r_i}\frac{\partial \hat{x}^s_i}{\partial x^r_j}
	\end{aligned}
\end{equation}

Due to the many additional derivatives involved in various applications of the chain rule we do each one separately. Next we have $\hat{x}^s_i$ w.r.t $x^r_j$. It is important to note that $\hat{x}$ contains $\mu$ which itself is dependent on $x$. Furthermore, $\hat{x}$ contains $\sigma^2$ which is dependent on $\mu$, which is dependent on $x$:
\begin{equation}
	\begin{aligned}
		\frac{\partial \hat{x}^s_i}{\partial x^r_j} &= \left[\left(\mathbb{I}\{s = r\}\mathbb{I}\{i = j\} - \frac{\partial \mu_s}{\partial x^r_j}\right)\left(\sigma_s^2 + \epsilon\right)^{\frac{1}{2}} - (x^s_i - \mu_s)\left(\frac{\frac{\partial \sigma_s^2}{\partial x^r_j}}{2(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\right)\right]\frac{1}{\sigma_s^2 + \epsilon} = \\ &= \left(\mathbb{I}\{s = r\}\mathbb{I}\{i = j\} - \frac{\partial \mu_s}{\partial x^r_j}\right)\left(\sigma_s^2 + \epsilon\right)^{-\frac{1}{2}} - (x^s_i - \mu_s)\left(\frac{\frac{\partial \sigma_s^2}{\partial x^r_j}}{2(\sigma_s^2 + \epsilon)^{\frac{3}{2}}}\right)
	\end{aligned}
\end{equation}

Next we compute the derivative of $\mu_s$ w.r.t $x^r_j$:

\begin{equation}
	\begin{aligned}
		\frac{\partial \mu_s}{\partial x^r_j} = \mathbb{I}\{s = r\}\frac{1}{B}
	\end{aligned}
\end{equation}

Next we compute $\sigma_s^2$ w.r.t $x^r_j$. Similarly to $\mu$ if $s \neq r$ the derivative is zero otherwise we have:
\begin{equation}
	\begin{aligned}
		\frac{\partial \sigma_s^2}{\partial x^r_j} &= \frac{1}{B}\sum_{i}\frac{\partial}{\partial x^s_j}(x^s_i - \mu_s)^2 = \frac{1}{B}\sum_{i}2(x^s_i - \mu_s)(\mathbb{I}\{i = j\} - \frac{1}{B}) = \\ &= \frac{2}{B}\sum_{i} x^s_i \mathbb{I}\{i = j\} - \frac{x^s_i}{B} - \mu_s \mathbb{I}\{i = j\} + \frac{\mu_s}{B}) =
		\\ &=
		\frac{2}{B}\left[\sum_{i} x^s_i\mathbb{I}\{i = j\} - \frac{1}{B}\sum_{i}x^s_i - \mu_s \sum_{i}\mathbb{I}\{i = j\} + \frac{1}{B}\mu_s \sum_{i}1\right] = \\ &= \frac{2}{B}\left[\sum_{i} x^s_i\mathbb{I}\{i = j\} - \mu_s - \mu_s + \mu_s\right] = \\ &= \frac{2}{B}\left(x^s_i - \mu_s\right) \\
		&\Rightarrow \\
		\frac{\partial \sigma_s^2}{\partial x^r_j} &= \mathbb{I}\{s = r\}\frac{2}{B}\left(x^s_i - \mu_s\right)
	\end{aligned}
\end{equation}

Now we can put it all together:

\begin{equation}
	\begin{aligned}
		\frac{\partial \hat{x}^s_i}{\partial x^r_j} &= \frac{\mathbb{I}\{s = r\}\mathbb{I}\{i = j\} - \frac{\partial \mu_s}{\partial x^r_j}}{\left(\sigma_s^2 + \epsilon\right)^{\frac{1}{2}}} - \frac{(x^s_i - \mu_s)\frac{\partial \sigma_s^2}{\partial x^r_j}}{2(\sigma_s^2 + \epsilon)^{\frac{3}{2}}} = \\ &= \frac{\mathbb{I}\{s = r\}\mathbb{I}\{i = j\}}{\left(\sigma_s^2 + \epsilon\right)^{\frac{1}{2}}} - \frac{\mathbb{I}\{s = r\}}{B\left(\sigma_s^2 + \epsilon\right)^{\frac{1}{2}}}- \frac{(x^s_i - \mu_s)\mathbb{I}\{s = r\}\left(x^s_i - \mu_s\right)}{B(\sigma_s^2 + \epsilon)^{\frac{1}{2}}(\sigma_s^2 + \epsilon)^{\frac{1}{2}}(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}= \\ &= \frac{\mathbb{I}\{s = r\}}{(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\left[\mathbb{I}\{i = j\} - \frac{1}{B} - \frac{(x^s_i - \mu_s)(x^s_i - \mu_s)}{B(\sigma_s^2 + \epsilon)^{\frac{1}{2}}(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\right] = \\ &= \frac{\mathbb{I}\{s = r\}}{(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\left[\mathbb{I}\{i = j\} - \frac{1}{B} - \frac{\hat{x}^s_i \hat{x}^s_i}{B}\right] \\
		&\Rightarrow \\
		\left(\frac{\partial L}{\partial x}\right)^r_j &= \sum_{s}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i}\frac{\mathbb{I}\{s = r\}}{(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\left[\mathbb{I}\{i = j\} - \frac{1}{B} - \frac{\hat{x}^s_i \hat{x}^s_i}{B}\right] = \\ &= \sum_{s}\frac{1}{(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i}\mathbb{I}\{s = r\}\mathbb{I}\{i = j\} - \frac{\mathbb{I}\{s = r\}}{B}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i} - \frac{\mathbb{I}\{s = r\}}{B}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i}x^s_ix^s_i = \\ &=\frac{1}{(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\left(\gamma_i\frac{\partial L}{\partial y^r_i} - \frac{1}{B}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i} - \frac{1}{B}\sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i}x^s_ix^s_i\right) = \\ &= \frac{1}{B(\sigma_s^2 + \epsilon)^{\frac{1}{2}}}\left(B\gamma_i\frac{\partial L}{\partial y^r_i} - \sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i} - \sum_{j}\gamma_i\frac{\partial L}{\partial y^r_i}x^s_ix^s_i\right)
	\end{aligned}
\end{equation}

\section{PyTorch CNN}

The accuracy and loss plots obtained from training the CNN can be found in \textit{Figure \ref{fig:cnn}}. As to be expected the CNN outperforms both MLP implementations, achieving an accuracy level of 78\%. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/cnn.png}
	\caption{Loss and Accuracy of CNN on test set}
	\label{fig:cnn}
\end{figure}

\end{document}
