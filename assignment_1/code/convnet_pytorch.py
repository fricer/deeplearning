"""
This module implements a Convolutional Neural Network in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
from torch import nn


class ConvNet(nn.Module):
    """
    This class implements a Convolutional Neural Network in PyTorch.
    It handles the different layers and parameters of the model.
    Once initialized an ConvNet object can perform forward.
    """

    def __init__(self, n_channels, n_classes):
        """
        Initializes ConvNet object.

        Args:
          n_channels: number of input channels
          n_classes: number of classes of the classification problem
        """
        super().__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes

        kernel = 3

        # pooling is always the same
        self.pooling = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.cnn = nn.Sequential(*[
            nn.Conv2d(n_channels, 64, kernel_size=kernel, stride=1, padding=1),  # conv1
            nn.BatchNorm2d(64),
            nn.ReLU(),
            self.pooling,                                                       # maxpool1
            nn.Conv2d(64, 128, kernel_size=kernel, stride=1, padding=1),        # conv2
            nn.BatchNorm2d(128),
            nn.ReLU(),
            self.pooling,                                                       # maxpool2
            nn.Conv2d(128, 256, kernel_size=kernel, stride=1, padding=1),       # conv3_a
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, kernel_size=kernel, stride=1, padding=1),       # conv3_b
            nn.BatchNorm2d(256),
            nn.ReLU(),
            self.pooling,                                                       # maxpool3
            nn.Conv2d(256, 512, kernel_size=kernel, stride=1, padding=1),       # conv4_a
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, kernel_size=kernel, stride=1, padding=1),       # conv4_b
            nn.BatchNorm2d(512),
            nn.ReLU(),
            self.pooling,                                                        # mapool4
            nn.Conv2d(512, 512, kernel_size=kernel, stride=1, padding=1),        # conv5_a
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, kernel_size=kernel, stride=1, padding=1),        # conv5_b
            nn.BatchNorm2d(512),
            nn.ReLU(),
            self.pooling                                             # maxppol5
        ])

        # needed after flattening of data
        self.linear = torch.nn.Linear(512, n_classes)


    def forward(self, x):
        """
        Performs forward pass of the input. Here an input tensor x is transformed through
        several layer transformations.

        Args:
          x: input to the network
        Returns:
          out: outputs of the network
        """

        out = self.cnn.forward(x)

        return self.linear(out.view(x.shape[0], -1))
