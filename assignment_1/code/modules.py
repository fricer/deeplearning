"""
This module implements various modules of the network.
You should fill in code into indicated sections.
"""
import numpy as np


class LinearModule(object):
  """
  Linear module. Applies a linear transformation to the input data. 
  """
  def __init__(self, in_features, out_features):
    """
    Initializes the parameters of the module. 
    
    Args:
      in_features: size of each input sample
      out_features: size of each output sample
    """
    self.params = {'weight': np.random.normal(loc=0.0, scale=0.0001, size=(out_features, in_features)),
                   'bias': np.zeros((1, out_features))}
    self.grads = {'weight': np.zeros((out_features, in_features)), 'bias': np.zeros((1, out_features))}

  def forward(self, x):
    """
    Forward pass.
    
    Args:
      x: input to the module
    Returns:
      out: output of the module
    """

    # save for backwards pass
    self.x = x

    # forward propagate by Wx + b
    out = np.dot(x, np.transpose(self.params["weight"])) + self.params["bias"]

    return out

  def backward(self, dout):
    """
    Backward pass.

    Args:
      dout: gradients of the previous module
    Returns:
      dx: gradients with respect to the input of the module
    """

    # compute gradients
    dx = np.dot(dout, self.params["weight"])

    # save in class instance
    self.grads["weight"] = np.dot(np.transpose(dout), self.x)
    self.grads["bias"] = np.sum(dout, axis=0)

    return dx


class LeakyReLUModule(object):
  """
  Leaky ReLU activation module.
  """
  def __init__(self, neg_slope):
    """
    Initializes the parameters of the module.

    Args:
      neg_slope: negative slope parameter.
    """
    self.neg_slope = neg_slope

  def forward(self, x):
    """
    Forward pass.
    
    Args:
      x: input to the module
    Returns:
      out: output of the module
    """

    # Definition of LeakyReLU
    out = np.maximum(0, x) + self.neg_slope * np.minimum(0, x)

    # Helpful for backwards pass

    # all elements greater than 0 are set ot 1
    # all others are 0
    self.positive_part = x > 0

    # flip positive part to easily compute leaky aspect
    self.leaky_part = self.positive_part == 0

    return out

  def backward(self, dout):
    """
    Backward pass.

    Args:
      dout: gradients of the previous module
    Returns:
      dx: gradients with respect to the input of the module
    """

    # compute gradient using elements saved in forward pass
    dx = dout * self.positive_part + self.neg_slope * (dout * self.leaky_part)

    return dx


class SoftMaxModule(object):
  """
  Softmax activation module.
  """

  def forward(self, x):
    """
    Forward pass.
    Args:
      x: input to the module
    Returns:
      out: output of the module
    """

    # get max value per datapoint
    max_x = np.amax(x, axis=1).reshape(x.shape[0], 1)

    # compute activations
    y = np.exp(x - max_x)

    # normalize
    out = y/y.sum(axis=1)[:,None]

    # save for backprop
    self.smax_out = out

    return out

  def backward(self, dout):
    """
    Backward pass.
    Args:
      dout: gradients of the previous module
    Returns:
      dx: gradients with respect to the input of the module
    """

    n_batches, output_size = self.smax_out.shape

    # the next two lines create a vector where each element is an
    # output_size x output_size diagonal matrix corresponding
    # to each row of self.smax_out
    diag = np.zeros([n_batches, output_size, output_size])
    np.einsum('abb->ab', diag)[...] = np.asarray([self.smax_out])

    # next line creates a vector where each element is an
    # output_size x output_size matrix produced by computing
    # the outer product of every row of self.smax_out with itself
    # iterative would be
    # np.outer(self.smax_out[i], self.smax_out[i])
    outer = np.einsum('ab, ac -> abc', self.smax_out, self.smax_out)

    # iterative would be
    # np.diag(self.smax_out[i]) - outer[i]
    diag_minus_outer = diag - outer

    # iterative would be
    # dx[i] = dout[i].dot(diag_minus_outer[i])
    dx = np.einsum('ab, abc -> ac', dout, diag_minus_outer)

    return dx


class CrossEntropyModule(object):
  """
  Cross entropy loss module.
  """
  def __init__(self):
    self.epsilon = 1e-10

  def forward(self, x, y):
    """
    Forward pass.
    Args:
      x: input to the module
      y: labels of the input
    Returns:
      out: cross entropy loss
    """

    # average of the sum of all results
    out = np.sum(-y * np.log(x + self.epsilon), axis=1).mean()

    return out

  def backward(self, x, y):
    """
    Backward pass.
    Args:
      x: input to the module
      y: labels of the input
    Returns:
      dx: gradient of the loss with the respect to the input x.
    """

    dx = -y / (x + self.epsilon) / len(x)

    return dx