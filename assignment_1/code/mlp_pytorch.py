"""
This module implements a multi-layer perceptron (MLP) in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from torch import nn


class MLP(nn.Module):
    """
    This class implements a Multi-layer Perceptron in PyTorch.
    It handles the different layers and parameters of the model.
    Once initialized an MLP object can perform forward.
    """

    def __init__(self, n_inputs, n_hidden, n_classes, neg_slope):
        """
        Initializes MLP object.

        Args:
          n_inputs: number of inputs.
          n_hidden: list of ints, specifies the number of units
                    in each linear layer. If the list is empty, the MLP
                    will not have any linear layers, and the model
                    will simply perform a multinomial logistic regression.
          n_classes: number of classes of the classification problem.
                     This number is required in order to specify the
                     output dimensions of the MLP
          neg_slope: negative slope parameter for LeakyReLU
        """
        # order of layers is the following
        # linear[i] -> activation[i] -> linear[i + 1] -> activation[i + 1] -> ...
        super().__init__()
        self.layers = []

        self.neg_slope = neg_slope

        # if list is empty do multinomial regression
        if not n_hidden:
            self.add_layer_and_activation(nn.Linear(n_inputs, n_classes), nn.Softmax(dim=1))

            self.num_of_layers = 1
        else:
            # list is not empty so at least one layer to add
            self.add_layer_and_activation(nn.Linear(n_inputs, n_hidden[0]), nn.LeakyReLU(neg_slope))

            # add any additional layers
            # without the last one
            for i in range(len(n_hidden) - 1):
                self.add_layer_and_activation(nn.Linear(n_hidden[i], n_hidden[i + 1]), nn.LeakyReLU(neg_slope))

            # add softmax layer
            # second variable passed here is none due to the nature of the cross entropy module of pytroch
            # already containing a softmax
            self.add_layer_and_activation(nn.Linear(n_hidden[len(n_hidden) - 1], n_classes), None)

            # save number of layers might be useful
            self.num_of_layers = len(self.layers)

        self.layers = nn.Sequential(*self.layers)

    def add_layer_and_activation(self, linear, activation):
        self.layers.append(linear)
        if activation is not None:
            self.layers.append(activation)

    def forward(self, x):
        """
        Performs forward pass of the input. Here an input tensor x is transformed through
        several layer transformations.

        Args:
          x: input to the network
        Returns:
          out: outputs of the network
        """

        out = self.layers.forward(x)

        return out
