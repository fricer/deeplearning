"""
This module implements training and evaluation of a Convolutional Neural Network in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import os
from convnet_pytorch import ConvNet
import cifar10_utils
import matplotlib.pyplot as plt
import torch

# Default constants
LEARNING_RATE_DEFAULT = 1e-4
BATCH_SIZE_DEFAULT = 32
MAX_STEPS_DEFAULT = 5000
EVAL_FREQ_DEFAULT = 500
OPTIMIZER_DEFAULT = 'ADAM'

# Directory in which cifar data is saved
DATA_DIR_DEFAULT = './cifar10/cifar-10-batches-py'

FLAGS = None

def accuracy(predictions, targets):
    """
    Computes the prediction accuracy, i.e. the average of correct predictions
    of the network.

    Args:
      predictions: 2D float array of size [batch_size, n_classes]
      labels: 2D int array of size [batch_size, n_classes]
              with one-hot encoding. Ground truth labels for
              each sample in the batch
    Returns:
      accuracy: scalar float, the accuracy of predictions,
                i.e. the average correct predictions over the whole batch
    """

    # same logic as with MLP pytorch
    if isinstance(predictions, torch.Tensor):
        predictions = predictions.detach().cpu().numpy()

    if isinstance(targets, torch.Tensor):
        targets = targets.detach().cpu().numpy()

    accuracy = np.mean(predictions.argmax(axis=1) == targets.argmax(axis=1))

    return accuracy

def train():
    """
    Performs training and evaluation of ConvNet model.
    """

    # set device to GPU if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    ### DO NOT CHANGE SEEDS!
    # Set the random seeds for reproducibility
    np.random.seed(42)

    # extract all params
    alpha = FLAGS.learning_rate
    max_steps = FLAGS.max_steps
    batch_size = FLAGS.batch_size
    eval_freq = FLAGS.eval_freq

    # get data
    data = cifar10_utils.get_cifar10(FLAGS.data_dir)

    # intialize cnn class
    # 3 channels, 10 n_classes
    cnn = ConvNet(3, 10)

    # again use GPU if possible
    cnn.to(device)

    # initialize optimizer
    optimizer = torch.optim.Adam(cnn.parameters(), lr=alpha)

    # initialize cross entropy module
    loss = torch.nn.CrossEntropyLoss().to(device)

    step_counter = 0

    # initialize empty list to keep track of accuracy
    accuracy_test = []

    # initialize empty list to keep track of loss
    loss_test = []

    # for plotting
    figure_x_axis = []

    # needed for testing otherwise test set does not fit in memory
    # number of batches of trainin set is examples / batch_size
    n_eval_batches = int(data['test']._num_examples / batch_size)

    for step in range(max_steps):

        # get next batch
        inputs, targets = data["train"].next_batch(batch_size)

        # needed so can be copied to gpu
        inputs = torch.from_numpy(inputs).to(device)
        targets = torch.from_numpy(targets).to(device)

        optimizer.zero_grad()

        # forward propagate
        output = cnn.forward(inputs)

        # compute gradient
        grad = loss.forward(output, targets.argmax(dim=1))

        # backpropagate
        grad.backward()
        optimizer.step()

        inputs.detach()
        targets.detach()
        output.detach()

        # evaluate network performance
        if step_counter % eval_freq == 0 or step_counter == max_steps - 1:

            current_loss = 0
            current_acc = 0

            for eval_batch in range(n_eval_batches):
                # get next batch
                inputs_test, targets_test = data["test"].next_batch(batch_size)

                # needed so can be copied to gpu
                inputs_test = torch.from_numpy(inputs_test).to(device)
                targets_test = torch.from_numpy(targets_test).to(device)

                test_output = cnn.forward(inputs_test)
                current_loss += loss.forward(test_output, targets_test.argmax(dim=1)).item()
                current_acc += accuracy(test_output, targets_test)

                # helps with memory mangement
                test_output.detach()
                inputs_test.detach()
                targets_test.detach()

            current_loss /= n_eval_batches
            current_acc /= n_eval_batches

            loss_test.append(current_loss)
            accuracy_test.append(current_acc)
            figure_x_axis.append(step_counter)

            print("Loss: %.3f\tAccuracy: %.3f\tTraining step: %d" % (current_loss, current_acc, step))

            # make plot and save for report
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.suptitle('CNN')
            ax1.set_title('CNN Loss over steps')
            ax1.plot(figure_x_axis, loss_test)
            ax2.set_title('CNN Accuracy over steps')
            ax2.plot(figure_x_axis, accuracy_test)
            fig.savefig('mlp_cnn.png', dpi=fig.dpi)

        step_counter += 1


def print_flags():
    """
    Prints all entries in FLAGS variable.
    """
    for key, value in vars(FLAGS).items():
        print(key + ' : ' + str(value))

def main():
    """
    Main function
    """
    # Print all Flags to confirm parameter settings
    print_flags()

    if not os.path.exists(FLAGS.data_dir):
        os.makedirs(FLAGS.data_dir)

    # Run the training operation
    train()

if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--learning_rate', type = float, default = LEARNING_RATE_DEFAULT,
                        help='Learning rate')
    parser.add_argument('--max_steps', type = int, default = MAX_STEPS_DEFAULT,
                        help='Number of steps to run trainer.')
    parser.add_argument('--batch_size', type = int, default = BATCH_SIZE_DEFAULT,
                        help='Batch size to run trainer.')
    parser.add_argument('--eval_freq', type=int, default=EVAL_FREQ_DEFAULT,
                        help='Frequency of evaluation on the test set')
    parser.add_argument('--data_dir', type = str, default = DATA_DIR_DEFAULT,
                        help='Directory for storing input data')
    FLAGS, unparsed = parser.parse_known_args()

    main()