"""
This module implements a multi-layer perceptron (MLP) in NumPy.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from modules import *


class MLP(object):
    """
    This class implements a Multi-layer Perceptron in NumPy.
    It handles the different layers and parameters of the model.
    Once initialized an MLP object can perform forward and backward.
    """

    def __init__(self, n_inputs, n_hidden, n_classes, neg_slope):
        """
        Initializes MLP object.

        Args:
          n_inputs: number of inputs.
          n_hidden: list of ints, specifies the number  of units
                    in each linear layer. If the list is empty, the MLP
                    will not have any linear layers, and the model
                    will simply perform a multinomial logistic regression.
          n_classes: number of classes of the classification problem.
                     This number is required in order to specify the
                     output dimensions of the MLP
          neg_slope: negative slope parameter for LeakyReLU
        """

        # order of layers is the following
        # linear[i] -> activation[i] -> linear[i + 1] -> activation[i + 1] -> ...
        self.layers = []

        # save neg slope value
        self.neg_slope = neg_slope

        # if list is empty do multinomial regression
        if not n_hidden:
            self.add_layer_and_activation(LinearModule(n_inputs, n_classes), SoftMaxModule())

            self.num_of_layers = 1
        else:

            # else populate the layers list

            # list is not empty so at least one layer to add
            self.add_layer_and_activation(LinearModule(n_inputs, n_hidden[0]), LeakyReLUModule(neg_slope))

            # add any additional layers
            # without the last one
            for i in range(len(n_hidden) - 1):
                self.add_layer_and_activation(LinearModule(n_hidden[i], n_hidden[i + 1]), LeakyReLUModule(neg_slope))

            # at the end of the loop add softmax layer
            self.add_layer_and_activation(LinearModule(n_hidden[len(n_hidden) -1], n_classes), SoftMaxModule())

            # save number of layers as it might come in handy
            self.num_of_layers = len(self.layers)

    def add_layer_and_activation(self, linear, activation):
        # add dictionary to list of layers
        self.layers.append({'Linear': linear, 'Activation': activation})

    def forward(self, x):
        """
        Performs forward pass of the input. Here an input tensor x is transformed through
        several layer transformations.

        Args:
          x: input to the network
        Returns:
          out: outputs of the network
        """

        for i in range(self.num_of_layers - 1):
            # forward propagate
            x = self.layers[i]["Linear"].forward(x)

            # compute activation
            x = self.layers[i]["Activation"].forward(x)

        # compute final activation
        out = self.layers[-1]["Activation"].forward(self.layers[-1]["Linear"].forward(x))
        return out

    def backward(self, dout):
        """
        Performs backward pass given the gradients of the loss.

        Args:
          dout: gradients of the loss
        """

        # backprop at least once in the case of mutlinomial distribution
        dout = self.layers[-1]["Linear"].backward(self.layers[-1]["Activation"].backward(dout))

        # backprop any additional layers
        # start from last layer
        for i in reversed(range(self.num_of_layers - 1)):

            # backward propagate activation
            dout = self.layers[i]["Activation"].backward(dout)

            # backward propagate linear
            dout = self.layers[i]["Linear"].backward(dout)

        return dout
