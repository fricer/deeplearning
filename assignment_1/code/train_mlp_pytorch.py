"""
This module implements training and evaluation of a multi-layer perceptron in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import os
from mlp_pytorch import MLP
import cifar10_utils
import matplotlib.pyplot as plt
import torch

# Default constants
DNN_HIDDEN_UNITS_DEFAULT = '100'
LEARNING_RATE_DEFAULT = 2e-3
MAX_STEPS_DEFAULT = 1500
BATCH_SIZE_DEFAULT = 200
EVAL_FREQ_DEFAULT = 100
NEG_SLOPE_DEFAULT = 0.02

# Directory in which cifar data is saved
DATA_DIR_DEFAULT = './cifar10/cifar-10-batches-py'

FLAGS = None


def accuracy(predictions, targets):
    """
    Computes the prediction accuracy, i.e. the average of correct predictions
    of the network.

    Args:
      predictions: 2D float array of size [batch_size, n_classes]
      labels: 2D int array of size [batch_size, n_classes]
              with one-hot encoding. Ground truth labels for
              each sample in the batch
    Returns:
      accuracy: scalar float, the accuracy of predictions,
                i.e. the average correct predictions over the whole batch
    """

    # detach if pytorch tensor
    if isinstance(predictions, torch.Tensor):
        predictions = predictions.detach().numpy()

    # detach if pytorch tensor
    if isinstance(targets, torch.Tensor):
        targets = targets.detach().numpy()

    # if both are detaches accuracy can be computed as before
    accuracy = np.mean(predictions.argmax(axis=1) == targets.argmax(axis=1))

    return accuracy

def train():
    """
    Performs training and evaluation of MLP model.
    """

    ### DO NOT CHANGE SEEDS!
    # Set the random seeds for reproducibility
    np.random.seed(42)

    ## Prepare all functions
    # Get number of units in each hidden layer specified in the string such as 100,100
    if FLAGS.dnn_hidden_units:
        dnn_hidden_units = FLAGS.dnn_hidden_units.split(",")
        dnn_hidden_units = [int(dnn_hidden_unit_) for dnn_hidden_unit_ in dnn_hidden_units]
    else:
        dnn_hidden_units = []

    # extract all params
    alpha = FLAGS.learning_rate
    max_steps = FLAGS.max_steps
    batch_size = FLAGS.batch_size
    eval_freq = FLAGS.eval_freq
    neg_slope = FLAGS.neg_slope

    data = cifar10_utils.get_cifar10(FLAGS.data_dir)

    train_set = data["train"]
    test_set = data["test"]

    (n_examples, height, width, depth) = train_set.images.shape
    n_outputs = train_set.labels.shape[1]

    test_inputs = torch.from_numpy(test_set.images.reshape([test_set.num_examples, -1]))
    test_labels = torch.from_numpy(test_set.labels)

    # initialize MLP
    mlp = MLP(height * width * depth, dnn_hidden_units, n_outputs, neg_slope)

    # add Adam optimizer
    optimizer = torch.optim.Adam(mlp.parameters(), lr=alpha)

    # initialize cross entropy module
    loss = torch.nn.CrossEntropyLoss()

    step_counter = 0

    # initialize empty list to keep track of accuracy
    accuracy_test = []

    # initialize empty list to keep track of loss
    loss_test = []

    # for plotting
    figure_x_axis = []

    for step in range(max_steps):
        # print("Epoch: %s" % epoch)
        # get one batch
        inputs, targets = train_set.next_batch(batch_size)

        # reshape
        inputs = torch.from_numpy(inputs.reshape([batch_size, -1]))

        targets = torch.LongTensor(np.argmax(targets,1))

        optimizer.zero_grad()

        # forward propagate
        output = mlp.forward(inputs)

        grad = loss(output, targets)

        # backprop
        grad.backward()

        # this time optmize using Adam optimizer
        optimizer.step()

        # evaluate network performance
        if step_counter % eval_freq == 0 or step_counter == max_steps - 1:
            # forward propagate test set
            test_output = mlp.forward(test_inputs)

            # compute loss
            current_loss = loss.forward(test_output, test_labels.argmax(dim=1))
            loss_test.append(float(current_loss.detach().numpy()))

            # compute accuracy
            current_accuracy = accuracy(test_output, test_labels)
            accuracy_test.append(current_accuracy)

            # update x-axis
            figure_x_axis.append(step_counter)

            print("Loss: %.3f\tAccuracy: %.3f\tTraining step: %d" % (current_loss, current_accuracy, step))

        step_counter += 1

    # make plot and save for report
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('MLP Pytorch')
    ax1.set_title('MLP Loss over steps')
    ax1.plot(figure_x_axis, loss_test)
    ax2.set_title('MLP Accuracy over steps')
    ax2.plot(figure_x_axis, accuracy_test)
    fig.savefig('mlp_pytorch.png', dpi=fig.dpi)

def print_flags():
    """
    Prints all entries in FLAGS variable.
    """
    for key, value in vars(FLAGS).items():
        print(key + ' : ' + str(value))

def main():
    """
    Main function
    """
    # Print all Flags to confirm parameter settings
    print_flags()

    if not os.path.exists(FLAGS.data_dir):
        os.makedirs(FLAGS.data_dir)

    # Run the training operation
    train()

if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--dnn_hidden_units', type = str, default = DNN_HIDDEN_UNITS_DEFAULT,
                        help='Comma separated list of number of units in each hidden layer')
    parser.add_argument('--learning_rate', type = float, default = LEARNING_RATE_DEFAULT,
                        help='Learning rate')
    parser.add_argument('--max_steps', type = int, default = MAX_STEPS_DEFAULT,
                        help='Number of steps to run trainer.')
    parser.add_argument('--batch_size', type = int, default = BATCH_SIZE_DEFAULT,
                        help='Batch size to run trainer.')
    parser.add_argument('--eval_freq', type=int, default=EVAL_FREQ_DEFAULT,
                        help='Frequency of evaluation on the test set')
    parser.add_argument('--data_dir', type = str, default = DATA_DIR_DEFAULT,
                        help='Directory for storing input data')
    parser.add_argument('--neg_slope', type=float, default=NEG_SLOPE_DEFAULT,
                        help='Negative slope parameter for LeakyReLU')
    FLAGS, unparsed = parser.parse_known_args()

    main()