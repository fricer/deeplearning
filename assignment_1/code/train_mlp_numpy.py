"""
This module implements training and evaluation of a multi-layer perceptron in NumPy.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import os
from mlp_numpy import MLP
from modules import CrossEntropyModule
import cifar10_utils
import matplotlib.pyplot as plt

# Default constants
DNN_HIDDEN_UNITS_DEFAULT = '100'
LEARNING_RATE_DEFAULT = 2e-3
MAX_STEPS_DEFAULT = 1500
BATCH_SIZE_DEFAULT = 200
EVAL_FREQ_DEFAULT = 100
NEG_SLOPE_DEFAULT = 0.02

# Directory in which cifar data is saved
DATA_DIR_DEFAULT = './cifar10/cifar-10-batches-py'

FLAGS = None


def accuracy(predictions, targets):
    """
    Computes the prediction accuracy, i.e. the average of correct predictions
    of the network.

    Args:
      predictions: 2D float array of size [batch_size, n_classes]
      labels: 2D int array of size [batch_size, n_classes]
              with one-hot encoding. Ground truth labels for
              each sample in the batch
    Returns:
      accuracy: scalar float, the accuracy of predictions,
                i.e. the average correct predictions over the whole batch
    """

    # fast numpy way of computing correct classifications
    return np.mean(predictions.argmax(axis=1) == targets.argmax(axis=1))


def train():
    """
    Performs training and evaluation of MLP model.
    """

    ### DO NOT CHANGE SEEDS!
    # Set the random seeds for reproducibility
    np.random.seed(42)

    ## Prepare all functions
    # Get number of units in each hidden layer specified in the string such as 100,100
    if FLAGS.dnn_hidden_units:
        dnn_hidden_units = FLAGS.dnn_hidden_units.split(",")
        dnn_hidden_units = [int(dnn_hidden_unit_) for dnn_hidden_unit_ in dnn_hidden_units]
    else:
        dnn_hidden_units = []

    # extract all params
    alpha = FLAGS.learning_rate
    max_steps = FLAGS.max_steps
    batch_size = FLAGS.batch_size
    eval_freq = FLAGS.eval_freq
    neg_slope = FLAGS.neg_slope

    # get the data
    data = cifar10_utils.get_cifar10(FLAGS.data_dir)

    # split in corresponding sets
    train_set = data["train"]
    test_set = data["test"]

    # extract necessary dimensions
    (n_examples, height, width, depth) = train_set.images.shape
    n_outputs = train_set.labels.shape[1]

    # prepare test set
    test_inputs = test_set.images.reshape([test_set.num_examples, -1])
    test_labels = test_set.labels

    # initialize MLP
    mlp = MLP(width * height * depth, dnn_hidden_units, n_outputs, neg_slope)

    # initialize cross entropy module
    loss = CrossEntropyModule()

    # counter to keep track of steps
    step_counter = 0

    # initialize empty list to keep track of accuracy
    accuracy_test = []

    # initialize empty list to keep track of loss
    loss_test = []

    # for plotting
    figure_x_axis = []

    # train for max steps
    for step in range(max_steps):

        # get one batch
        inputs, targets = train_set.next_batch(batch_size)

        # reshape
        inputs = inputs.reshape([batch_size, -1])

        # forward propagate
        output = mlp.forward(inputs)

        # calculate grad
        grad = loss.backward(output, targets)

        # propagate back
        mlp.backward(grad)

        # update weights and biases manually
        # without the use of an optimizer
        for layer in mlp.layers:
            module = layer["Linear"]
            module.params["weight"] -= alpha * module.grads["weight"]
            module.params["bias"] -= alpha * module.grads["bias"]

        # evaluate network performance at each eval_freq AND before exiting
        if step_counter % eval_freq == 0 or step_counter == max_steps - 1:

            # forward propagate test set
            test_output = mlp.forward(test_inputs)

            # compute loss
            current_loss = loss.forward(test_output, test_labels)
            loss_test.append(current_loss)

            # compute accuracy
            current_acc = accuracy(test_output, test_labels)
            accuracy_test.append(current_acc)

            # update x-axis
            figure_x_axis.append(step_counter)

            # print some visual stuff
            print("Loss: %.3f\tAccuracy: %.3f\tTraining step: %d" % (current_loss, current_acc, step))

        # end of step update counter
        step_counter += 1

    # make plot and save for report
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('MLP Numpy')
    ax1.set_title('MLP Loss over steps')
    ax1.plot(figure_x_axis, loss_test)
    ax2.set_title('MLP Accuracy over steps')
    ax2.plot(figure_x_axis, accuracy_test)
    fig.savefig('mlp_numpy.png', dpi=fig.dpi)

def print_flags():
    """
    Prints all entries in FLAGS variable.
    """
    for key, value in vars(FLAGS).items():
        print(key + ' : ' + str(value))


def main():
    """
    Main function
    """
    # Print all Flags to confirm parameter settings
    print_flags()

    if not os.path.exists(FLAGS.data_dir):
        os.makedirs(FLAGS.data_dir)

    # Run the training operation
    train()


if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--dnn_hidden_units', type = str, default = DNN_HIDDEN_UNITS_DEFAULT,
                        help='Comma separated list of number of units in each hidden layer')
    parser.add_argument('--learning_rate', type = float, default = LEARNING_RATE_DEFAULT,
                        help='Learning rate')
    parser.add_argument('--max_steps', type = int, default = MAX_STEPS_DEFAULT,
                        help='Number of steps to run trainer.')
    parser.add_argument('--batch_size', type = int, default = BATCH_SIZE_DEFAULT,
                        help='Batch size to run trainer.')
    parser.add_argument('--eval_freq', type=int, default=EVAL_FREQ_DEFAULT,
                        help='Frequency of evaluation on the test set')
    parser.add_argument('--data_dir', type = str, default = DATA_DIR_DEFAULT,
                        help='Directory for storing input data')
    parser.add_argument('--neg_slope', type=float, default=NEG_SLOPE_DEFAULT,
                        help='Negative slope parameter for LeakyReLU')
    FLAGS, unparsed = parser.parse_known_args()

    main()