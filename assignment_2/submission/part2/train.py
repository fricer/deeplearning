# MIT License
#
# Copyright (c) 2019 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time
from datetime import datetime
import argparse
import random

import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.optim as optim
from torch.utils.data import DataLoader

import sys
sys.path.append('.')
sys.path.append('..')

from part2.dataset import TextDataset
from part2.model import TextGenerationModel

################################################################################


def train(config):

    lstm_acc = []
    lstm_loss = []


    # Initialize the device which to run the model on
    device = torch.device(config.device)

    # Initialize the dataset and data loader (note the +1)
    dataset = TextDataset(config.txt_file, config.seq_length)
    data_loader = DataLoader(dataset, config.batch_size, num_workers=1)

    # Initialize the model that we are going to use
    model = TextGenerationModel(config.batch_size, config.seq_length, dataset.vocab_size, config.lstm_num_hidden,
                                config.lstm_num_layers, device)

    # Setup the loss and optimizer
    criterion = torch.nn.CrossEntropyLoss().to(device)
    optimizer = optim.RMSprop(model.parameters(), lr=config.learning_rate)

    epoch_step = 0

    # fixed loop
    while epoch_step < config.train_steps:

        for step, (batch_inputs, batch_targets) in enumerate(data_loader):

            # Only for time measurement of step through network
            t1 = time.time()

            inputs = torch.nn.functional.one_hot(batch_inputs.to(torch.int64), num_classes=dataset.vocab_size).float().to(device)

            targets = batch_targets.to(device)

            # zero out gradients
            optimizer.zero_grad()

            # forward propagate
            output = model.forward(inputs)

            torch.nn.utils.clip_grad_norm(model.parameters(), max_norm=config.max_norm)

            # compute loss
            loss = criterion(output.transpose(1,2), targets)
            lstm_loss.append(loss.item())

            # backprop
            loss.backward()

            # step through optimizer
            optimizer.step()

            accuracy = (output.argmax(dim=-1) == targets).float().mean()
            lstm_acc.append(accuracy.item())

            # Just for time measurement
            t2 = time.time()
            examples_per_second = config.batch_size/float(t2-t1)

            if step % config.print_every == 0:

                print("[{}] Train Step {:04d}/{:04d}, Batch Size = {}, Examples/Sec = {:.2f}, "
                      "Accuracy = {:.2f}, Loss = {:.3f}".format(
                        datetime.now().strftime("%Y-%m-%d %H:%M"), epoch_step,
                        int(config.train_steps), config.batch_size, examples_per_second,
                        accuracy, loss
                ))

            if step % config.sample_every == 0:

                with torch.no_grad():

                	# make epty tensor with length of seq_length
                    x = torch.zeros(1, config.seq_length).to(device)

                    # pick random char from dictionary
                    next_char = random.randint(0, dataset.vocab_size - 1)

                    # to keep track of all chars
                    chars = []

                    for i in range(0, config.seq_length):

                    	# add char to sequence
                        x[0][i] = float(next_char)
                        chars.append(next_char)

                        # convert to one hot
                        x_one_hot = torch.nn.functional.one_hot(x.to(torch.int64), num_classes=dataset.vocab_size).float().to(device)

                        # get output
                        y = model(x_one_hot)[:, i, :].squeeze()

                        # get softmax distribution
                        dist = torch.softmax(y.to(torch.float) / config.temp, dim=0)

                        # sample according to softmax
                        next_char = torch.multinomial(dist, 1).item()

                    # get text output
                    text = dataset.convert_to_string(chars)

                    # add to file
                    with open(config.summary_path + "rand_length_" + str(config.seq_length) + "_temp_" + str(int(config.temp * 10)) + ".txt", "a+") as file:
                        file.write(text + "\n")

                    # plotting
                    xaxis = [x for x in range(0, epoch_step + 1)]
                    fig, (ax1, ax2) = plt.subplots(1, 2)
                    fig.suptitle('LSTM. seq_length: ' + str(config.seq_length) 
                        + ", temp: " + str(config.temp))
                    ax1.set_title('LSTM Accuracy')
                    ax1.plot(xaxis, lstm_acc)
                    ax2.set_title('LSTM Loss')
                    ax2.plot(xaxis, lstm_loss)
                    fig.savefig('lstm_random_text_' + str(config.seq_length) + '_' + str(int(config.temp * 10)) +  '.png', dpi=fig.dpi)

            if step == config.train_steps:
                break

            epoch_step += 1

    print('Done training.')


 ################################################################################
 ################################################################################

if __name__ == "__main__":

    # Parse training configuration
    parser = argparse.ArgumentParser()

    # Model params
    parser.add_argument('--txt_file', type=str, required=True, help="Path to a .txt file to train on")
    parser.add_argument('--seq_length', type=int, default=30, help='Length of an input sequence')
    parser.add_argument('--lstm_num_hidden', type=int, default=128, help='Number of hidden units in the LSTM')
    parser.add_argument('--lstm_num_layers', type=int, default=2, help='Number of LSTM layers in the model')

    # Training params
    parser.add_argument('--batch_size', type=int, default=64, help='Number of examples to process in a batch')
    parser.add_argument('--learning_rate', type=float, default=2e-3, help='Learning rate')

    # It is not necessary to implement the following three params, but it may help training.
    parser.add_argument('--learning_rate_decay', type=float, default=0.96, help='Learning rate decay fraction')
    parser.add_argument('--learning_rate_step', type=int, default=5000, help='Learning rate step')
    parser.add_argument('--dropout_keep_prob', type=float, default=1.0, help='Dropout keep probability')

    parser.add_argument('--train_steps', type=int, default=1e6, help='Number of training steps')
    parser.add_argument('--max_norm', type=float, default=5.0, help='--')

    # Misc params
    parser.add_argument('--summary_path', type=str, default="./summaries/", help='Output path for summaries')
    parser.add_argument('--print_every', type=int, default=5, help='How often to print training progress')
    parser.add_argument('--sample_every', type=int, default=100, help='How often to sample from the model')

    # was missing
    parser.add_argument('--device', type=str, default="cuda:0", help="Training device 'cpu' or 'cuda:0'")

    parser.add_argument('--temp', type=float, default=1, help="Temperature value for softmax")

    config = parser.parse_args()

    # Train the model
    train(config)
