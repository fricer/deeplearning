################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn

import argparse
import time
from datetime import datetime
import numpy as np

from torch.utils.data import DataLoader

import sys
sys.path.append('..')

from part1.dataset import PalindromeDataset

import matplotlib.pyplot as plt

################################################################################
class LSTM(nn.Module):

    def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu'):
        super(LSTM, self).__init__()
        # Initialization here ...
        self.seq_length = seq_length
        self.device = device
        self.num_hidden = num_hidden
        self.input_dim = input_dim

        # weights for input x
        self.W_gx = nn.Parameter(torch.randn(input_dim, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_gx)

        self.W_ix = nn.Parameter(torch.randn(input_dim, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_ix)

        self.W_fx = nn.Parameter(torch.randn(input_dim, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_fx)

        self.W_ox = nn.Parameter(torch.randn(input_dim, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_ox)

        # weights for hidden h
        self.W_gh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_gh)

        self.W_ih = nn.Parameter(torch.randn(num_hidden, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_ih)

        self.W_fh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_fh)

        self.W_oh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        torch.nn.init.xavier_uniform_(self.W_oh)

        # biases
        self.b_g = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_i = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_f = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_o = nn.Parameter(torch.zeros(1, num_hidden))

        # weights and biases for hidden to output
        self.W_ph = nn.Parameter(torch.randn(num_hidden, num_classes))
        torch.nn.init.xavier_uniform_(self.W_ph)
        
        self.b_p = nn.Parameter(torch.zeros(1, num_classes))

        self.to(device)
        self.grads = []

    def forward(self, x):

        # get batch size
        batch = x.shape[0]
        self.grads = []

        # init c and h
        c = torch.zeros((batch, self.num_hidden)).to(self.device)
        h = torch.zeros((batch, self.num_hidden)).to(self.device)

        for t in range(self.seq_length):

            # input at time step t
            if self.input_dim == 10:
                # for one hots
                x_t = x[:, t, :]
            else:
                x_t = x[:, t].reshape(-1, 1)

            # input modulation gate
            g_t = torch.tanh(x_t @ self.W_gx + h @ self.W_gh + self.b_g)

            # input gate
            i_t = torch.sigmoid(x_t @ self.W_ix + h @ self.W_ih + self.b_i)

            # forgetting gate
            f_t = torch.sigmoid(x_t @ self.W_fx + h @ self.W_fh + self.b_f)

            # output gate
            o_t = torch.sigmoid(x_t @ self.W_ox + h @ self.W_oh + self.b_o)

            # cell
            c = g_t * i_t + c * f_t

            # hidden
            h = torch.tanh(c) * o_t

            h.requires_grad_(True)
            h.retain_grad()

            self.grads.append(h)

        p = h @ self.W_ph + self.b_p

        return torch.softmax(p, dim=1)


class VanillaRNN(nn.Module):

    def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu'):
        super(VanillaRNN, self).__init__()
        # Initialization here ...

        # number of time steps. Needed for forward pass
        self.seq_length = seq_length
        self.device = device
        self.num_hidden = num_hidden
        self.input_dim = input_dim

        # init weights and biases

        # weights
        self.W_hx = nn.Parameter(torch.randn(input_dim, num_hidden))
        self.W_hh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        self.W_ph = nn.Parameter(torch.randn(num_hidden, num_classes))

        # biases
        self.b_h = nn.Parameter(torch.zeros(1,num_hidden))
        self.b_p = nn.Parameter(torch.zeros(1,num_classes))

        # Run xavier init
        torch.nn.init.xavier_uniform_(self.W_hx)
        torch.nn.init.xavier_uniform_(self.W_hh)
        torch.nn.init.xavier_uniform_(self.W_ph)

        self.to(device)
        self.grads = []

    def forward(self, x):

        # get batch size
        batch = x.shape[0]
        self.grads = []

        # init h
        h = torch.zeros((batch, self.num_hidden)).to(self.device)
        for t in range(self.seq_length):

            if self.input_dim == 10:
                # for one hots
                x_t = x[:, t, :]
            else:
                x_t = x[:, t].reshape(-1, 1)

            h =torch.tanh(x_t @ self.W_hx + h @ self.W_hh + self.b_h)

            h.requires_grad_(True)
            h.retain_grad()

            self.grads.append(h)

        return h @ self.W_ph + self.b_p

def train(config):

    # seed = int(round((time.time() * 1e+9)) % 1e+8)
    seed = 0
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

    # Initialize the device which to run the model on
    device = torch.device(config.device)

    rnn = VanillaRNN(config.input_length, config.input_dim, config.num_hidden, config.num_classes, device)
    lstm = LSTM(config.input_length, config.input_dim, config.num_hidden, config.num_classes, device)

    # Initialize the dataset and data loader (note the +1)
    dataset = PalindromeDataset(config.input_length+1)
    data_loader = DataLoader(dataset, config.batch_size, num_workers=1)

    # Setup the loss and optimizer
    criterion = torch.nn.CrossEntropyLoss().to(device)

    batch_inputs, batch_targets = next(iter(data_loader))

    # use one hots
    if config.input_dim == 10:
        batch_inputs = torch.nn.functional.one_hot(batch_inputs.to(torch.int64)).type(torch.FloatTensor).to(device)

    # send tensors to gpu
    batch_inputs = batch_inputs.to(device)
    batch_targets = batch_targets.to(device)


    # forward propagate
    rnn_out = rnn(batch_inputs)

    # compute loss
    rnn_loss = criterion(rnn_out, batch_targets)

    # zero out gradients
    rnn.zero_grad()

    rnn_loss.backward()

    mags_rnn = [torch.norm(h.grad) for h in rnn.grads]

    plt.plot(list(range(config.input_length)), mags_rnn, label="RNN")

    # forward propagate
    lstm_out = lstm(batch_inputs)

    # compute loss
    lstm_loss = criterion(lstm_out, batch_targets)

    # zero out gradients
    lstm.zero_grad()

    lstm_loss.backward()

    mags = [torch.norm(h.grad) for h in lstm.grads]

    plt.plot(list(range(config.input_length)), mags, label="LSTM")
    plt.legend()
    plt.show()

if __name__ == "__main__":

    # Parse training configuration
    parser = argparse.ArgumentParser()

    # Model params
    parser.add_argument('--input_length', type=int, default=10, help='Length of an input sequence')
    parser.add_argument('--input_dim', type=int, default=1, help='Dimensionality of input sequence')
    parser.add_argument('--num_classes', type=int, default=10, help='Dimensionality of output sequence')
    parser.add_argument('--num_hidden', type=int, default=128, help='Number of hidden units in the model')
    parser.add_argument('--batch_size', type=int, default=128, help='Number of examples to process in a batch')
    parser.add_argument('--learning_rate', type=float, default=0.001, help='Learning rate')
    parser.add_argument('--train_steps', type=int, default=10000, help='Number of training steps')
    parser.add_argument('--max_norm', type=float, default=10.0)
    parser.add_argument('--device', type=str, default="cuda:0", help="Training device 'cpu' or 'cuda:0'")

    config = parser.parse_args()

    train(config)
