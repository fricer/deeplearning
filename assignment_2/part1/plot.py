import numpy as np
import matplotlib.pyplot as plt

LENGTH = 1001

x_axis = np.asarray([x for x in range(0, 10001, 10)])

x_axis_p = [5,10,11,12,13,14,15]

fig2, (a1, a2) = plt.subplots(1, 2)
fig2.suptitle('Max accuracy and Min loss of RNN and LSTM')
a1.set_title('Max accuracy')
a2.set_title('Min loss')


for network in ["RNN", "LSTM"]:

	max_acc = []
	min_loss = []

	fig, (ax1, ax2) = plt.subplots(1, 2)
	fig.suptitle('Accuracy and Loss for ' + network)
	ax1.set_title(network + ' Accuracy over steps')
	ax2.set_title(network + ' Loss over steps')

	folder_name = network + "_files/"

	for length in x_axis_p:

		acc_avg = np.zeros(LENGTH)
		loss_avg = np.zeros(LENGTH) 

		for trial in range(1,4):
			file = folder_name + network + "_length_" + str(length) + "_trial_" + str(trial) 
			print(file)

			acc_avg += np.load(file + "_acc.npy")
			loss_avg += np.load(file + "_loss.npy")

		acc_avg = acc_avg / 3
		loss_avg = loss_avg / 3
		ax1.plot(x_axis, acc_avg, label="T = " + str(length))
		ax2.plot(x_axis, loss_avg, label="T = " + str(length))

		max_acc.append(np.max(acc_avg))
		min_loss.append(np.min(loss_avg))

	a1.plot(x_axis_p, max_acc, label=network)
	a1.legend()
	plt.sca(a1)
	plt.xticks(np.arange(5, 16, step=1))
	a1.set_xlabel("Palindrome length")
	a1.set_ylabel("Max accuracy for 10000 steps")
	a2.plot(x_axis_p, min_loss, label=network)
	a2.legend()
	plt.sca(a2)
	plt.xticks(np.arange(5, 16, step=1))
	a2.set_xlabel("Palindrome length")
	a2.set_ylabel("Min loss for 10000 steps")

	ax1.legend()
	ax1.set_xlabel("Steps")
	ax1.set_ylabel("Accuracy")

	ax2.legend()
	ax2.set_xlabel("Steps")
	ax2.set_ylabel("Loss")
	
	fig.savefig(network + '.png', dpi=fig.dpi)

fig2.savefig('summary.png', dpi=fig.dpi)



