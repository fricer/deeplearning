################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn

################################################################################

class LSTM(nn.Module):

    def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu'):
        super(LSTM, self).__init__()
        # Initialization here ...
        self.seq_length = seq_length
        self.device = device
        self.num_hidden = num_hidden
        self.input_dim = input_dim

        # weights for input x
        self.W_gx = nn.Parameter(torch.randn(input_dim, num_hidden))
        self.W_ix = nn.Parameter(torch.randn(input_dim, num_hidden))
        self.W_fx = nn.Parameter(torch.randn(input_dim, num_hidden))
        self.W_ox = nn.Parameter(torch.randn(input_dim, num_hidden))

        # weights for hidden h
        self.W_gh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        self.W_ih = nn.Parameter(torch.randn(num_hidden, num_hidden))
        self.W_fh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        self.W_oh = nn.Parameter(torch.randn(num_hidden, num_hidden))

        # biases
        self.b_g = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_i = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_f = nn.Parameter(torch.zeros(1, num_hidden))
        self.b_o = nn.Parameter(torch.zeros(1, num_hidden))

        # weights and biases for hidden to output
        self.W_ph = nn.Parameter(torch.randn(num_hidden, num_classes))
        self.b_p = nn.Parameter(torch.zeros(1, num_classes))

        self.to(device)

    def forward(self, x):

        # get batch size
        batch = x.shape[0]

        # init c and h
        c = torch.zeros((batch, self.num_hidden)).to(self.device)
        h = torch.zeros((batch, self.num_hidden)).to(self.device)

        for t in range(self.seq_length):

            # input at time step t
            if self.input_dim == 10:
                # for one hots
                x_t = x[:, t, :]
            else:
                x_t = x[:, t].reshape(-1, 1)

            # input modulation gate
            g_t = torch.tanh(x_t @ self.W_gx + h @ self.W_gh + self.b_g)

            # input gate
            i_t = torch.sigmoid(x_t @ self.W_ix + h @ self.W_ih + self.b_i)

            # forgetting gate
            f_t = torch.sigmoid(x_t @ self.W_fx + h @ self.W_fh + self.b_f)

            # output gate
            o_t = torch.sigmoid(x_t @ self.W_ox + h @ self.W_oh + self.b_o)

            # cell
            c = g_t * i_t + c * f_t

            # hidden
            h = torch.tanh(c) * o_t

        p = h @ self.W_ph + self.b_p

        return torch.softmax(p, dim=1)
