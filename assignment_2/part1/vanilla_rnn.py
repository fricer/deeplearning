################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn

################################################################################


class VanillaRNN(nn.Module):

    def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu'):
        super(VanillaRNN, self).__init__()
        # Initialization here ...

        # number of time steps. Needed for forward pass
        self.seq_length = seq_length
        self.device = device
        self.num_hidden = num_hidden
        self.input_dim = input_dim

        # init weights and biases

        # weights
        self.W_hx = nn.Parameter(torch.randn(input_dim, num_hidden))
        self.W_hh = nn.Parameter(torch.randn(num_hidden, num_hidden))
        self.W_ph = nn.Parameter(torch.randn(num_hidden, num_classes))

        # biases
        self.b_h = nn.Parameter(torch.zeros(1,num_hidden))
        self.b_p = nn.Parameter(torch.zeros(1,num_classes))

        self.to(device)

    def forward(self, x):

        # get batch size
        batch = x.shape[0]

        # init h
        h = torch.zeros((batch, self.num_hidden)).to(self.device)
        for t in range(self.seq_length):

            if self.input_dim == 10:
                # for one hots
                x_t = x[:, t, :]
            else:
                x_t = x[:, t].reshape(-1, 1)

            h = torch.tanh(x_t @ self.W_hx + h @ self.W_hh + self.b_h)

        return h @ self.W_ph + self.b_p
