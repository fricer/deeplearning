################################################################################
# MIT License
# 
# Copyright (c) 2019
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import time
from datetime import datetime
import numpy as np

import torch
from torch.utils.data import DataLoader

import sys
sys.path.append('.')
sys.path.append('..')

from part1.dataset import PalindromeDataset
from part1.vanilla_rnn import VanillaRNN
from part1.lstm import LSTM

# You may want to look into tensorboard for logging
# from torch.utils.tensorboard import SummaryWriter

################################################################################


def train(config):

    seed = int(round((time.time() * 1e+9)) % 1e+8)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

    assert config.model_type in ('RNN', 'LSTM')

    # Initialize the device which to run the model on
    device = torch.device(config.device)

    # Initialize the model that we are going to use
    model = None
    if config.model_type == "RNN":
        model = VanillaRNN(config.input_length, config.input_dim, config.num_hidden, config.num_classes, device)
    else:
        model = LSTM(config.input_length, config.input_dim, config.num_hidden, config.num_classes, device)

    # Initialize the dataset and data loader (note the +1)
    dataset = PalindromeDataset(config.input_length+1)
    data_loader = DataLoader(dataset, config.batch_size, num_workers=1)

    # Setup the loss and optimizer
    criterion = torch.nn.CrossEntropyLoss().to(device)
    optimizer = torch.optim.RMSprop(model.parameters(), lr=config.learning_rate)

    # for plotting
    loss_over_t = []
    acc_over_t = []

    for step, (batch_inputs, batch_targets) in enumerate(data_loader):
        
        # Only for time measurement of step through network
        t1 = time.time()

        # Add more code here ...

        # use one hots
        if config.input_dim == 10:
            batch_inputs = torch.nn.functional.one_hot(batch_inputs.to(torch.int64)).type(torch.FloatTensor).to(device)

        # send tensors to gpu
        batch_inputs = batch_inputs.to(device)
        batch_targets = batch_targets.to(device)

        # forward propagate
        output = model.forward(batch_inputs)

        # compute loss
        loss = criterion(output, batch_targets)

        # zero out gradients
        optimizer.zero_grad()

        # backprop
        loss.backward()

        ############################################################################
        # QUESTION: what happens here and why?
        #
        # limits gradient magnitudes
        ############################################################################
        torch.nn.utils.clip_grad_norm(model.parameters(), max_norm=config.max_norm)
        ############################################################################

        # step through optimizer
        optimizer.step()

        accuracy = (output.argmax(dim=1) == batch_targets).float().mean()

        # Just for time measurement
        t2 = time.time()
        examples_per_second = config.batch_size/float(t2-t1)

        if step % 10 == 0:

            print("[{}] Train Step {:04d}/{:04d}, Batch Size = {}, Examples/Sec = {:.2f}, "
                  "Accuracy = {:.2f}, Loss = {:.3f}".format(
                    datetime.now().strftime("%Y-%m-%d %H:%M"), step,
                    config.train_steps, config.batch_size, examples_per_second,
                    accuracy, loss
            ))
            loss_over_t.append(loss.item())
            acc_over_t.append(accuracy.item())

        if step == config.train_steps:
            # If you receive a PyTorch data-loader error, check this bug report:
            # https://github.com/pytorch/pytorch/pull/9655
            break

    file_name = str(config.model_type) + "_length_" + str(config.input_length) + "_trial_" + str(config.trial_num)

    np.save(file_name + "_loss", loss_over_t)
    np.save(file_name + "_acc", acc_over_t)

    print('Done training.')


 ################################################################################
 ################################################################################

if __name__ == "__main__":

    # Parse training configuration
    parser = argparse.ArgumentParser()

    # Model params
    parser.add_argument('--model_type', type=str, default="RNN", help="Model type, should be 'RNN' or 'LSTM'")
    parser.add_argument('--input_length', type=int, default=10, help='Length of an input sequence')
    parser.add_argument('--input_dim', type=int, default=1, help='Dimensionality of input sequence')
    parser.add_argument('--num_classes', type=int, default=10, help='Dimensionality of output sequence')
    parser.add_argument('--num_hidden', type=int, default=128, help='Number of hidden units in the model')
    parser.add_argument('--batch_size', type=int, default=128, help='Number of examples to process in a batch')
    parser.add_argument('--learning_rate', type=float, default=0.001, help='Learning rate')
    parser.add_argument('--train_steps', type=int, default=10000, help='Number of training steps')
    parser.add_argument('--max_norm', type=float, default=10.0)
    parser.add_argument('--device', type=str, default="cuda:0", help="Training device 'cpu' or 'cuda:0'")

    # for keeping track of num of trials per input_length
    parser.add_argument('--trial_num', type=int, default=1, help="Trial number for give input_length")

    config = parser.parse_args()

    # Train the model
    train(config)
