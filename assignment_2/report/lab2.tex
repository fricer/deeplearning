\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2018

% ready for submission
% \usepackage{neurips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
     \usepackage[final]{style}

% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{neurips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amssymb}

\renewcommand{\thesubsubsection}{\thesubsection.\alph{subsubsection}}

\title{Deep Learning\\Assignment 2. Recurrent Neural Networks and Graph Neural Networks}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{Ivan Yovchev\\
	12871737\\
	yovchev23@gmail.com
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

\section{Vanilla RNN versus LSTM}

\subsection*{Question 1.1}

Then for $\mathcal{L}_t$ w.r.t $W_{ph}$ for one time step we have:
\begin{equation}
	\begin{aligned}
		\frac{\partial \mathcal{L}_t}{\partial W_{ph}} = \frac{\partial \mathcal{L}_t}{\partial \hat{y}_{t_k}}\frac{\partial \hat{y}_{t_k}}{\partial h_t}\frac{\partial h_t}{\partial W_{ph_{ij}}} = (\hat{y}_{t_k} - y_t) \otimes h_t
	\end{aligned}
\end{equation}

For all time steps we then have:
\begin{equation}
	\frac{\partial \mathcal{L}}{\partial W_{ph}} = \sum_{t} \frac{\partial \mathcal{L}_t}{\partial W_{ph}}
\end{equation}

Then for $\mathcal{L}_t$ w.r.t $W_{hh}$ we have the following:

\begin{equation}
	\begin{aligned}
			\frac{\partial \mathcal{L}_t}{\partial W_{hh}} &= \frac{\partial \mathcal{L}_t}{\partial \hat{y}_{t_k}}\frac{\partial \hat{y}_{t_k}}{\partial h_t}\frac{\partial h_t}{\partial W_{hh}} = \frac{\partial \mathcal{L}_t}{\partial \hat{y}_{t_k}}\frac{\partial \hat{y}_{t_k}}{\partial h_t}\frac{\partial h_t}{\partial h_k}\frac{\partial h_k}{\partial W_{hh}} = \\ &= \frac{\partial \mathcal{L}_t}{\partial \hat{y}_{t_k}}\frac{\partial \hat{y}_{t_k}}{\partial h_t}\left(\prod_{j = k + 1}^{t}\frac{\partial h_j}{\partial h_{j - 1}}\right)\frac{\partial h_k}{\partial W_{hh}}
	\end{aligned}
	\label{eq:3}
\end{equation}

For all times steps we then have:
\begin{equation}
		\frac{\partial \mathcal{L}}{\partial W_{hh}} = \sum_{t} \frac{\partial \mathcal{L}_t}{\partial W_{hh}}
\end{equation}

The $\prod_{j = k + 1}^{t}\frac{\partial h_j}{\partial h_{j - 1}}$ term in \eqref{eq:3} is problematic as if the terms $\frac{\partial h_t}{\partial h_{t-1}} < 1$ then the whole product $<< 1$, making the fraction $\frac{\partial \mathcal{L}_t}{\partial W_{hh}} << 1$. This is know as the vanishing gradient problem. If we have the opposite and the terms $\frac{\partial h_t}{\partial h_{t-1}} > 1$ then $\frac{\partial \mathcal{L}_t}{\partial W_{hh}} >> 1$, known as the exploding gradient problem.

\subsection*{Question 1.2}

The implementation can be found in the included files \verb|vanilla_rnn.py| and \verb|train.py|

\subsection*{Question 1.3}

An experiment was performed to evaluate the accuracy of the RNN using palindromes of varying length (palindrome length is referred to as T). In order to obtain average results over multiple trials for each length T the RNN performance was evaluated 3 times and the average results were plotted in \textit{Figure \ref{fig:rnn_acc}}. All of the conducted experiments use one-hot-encoding  of the inputs as testing showed it leads to an overall faster converging of the network. All other input parameters where kept at their pre-specified defaults (see \verb|train.py|)

From the figure it can be seen that the network achieves a perfect score for $T=5$ with a corresponding loss of 0 in a very few number of steps. Doubling the palindrome length to $T = 10$ leads to similar results but it takes the network around 10000 steps to do so. Any further increases in the palindrome length leads to a deterioration of the overall performance in the provided number of time steps. For $T = 11$ the accuracy drops to 0.85 and any further increases in palindrome length negatively impacts accuracy, dropping all the way down to 0.4 for $T = 15$ with an average loss of around 2.5.
  
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/RNN.png}
	\caption{Accuracy and Loss of RNN for different T values over time}
	\label{fig:rnn_acc}
\end{figure}

\subsection*{Question 1.4}

There are several problems associated with the use of vanilla stochastic gradient descent. For example, while training the algorithm may encounter what is known as a \textit{pathological curvature}. This curvature represents a part of the parameters space which has the form of a crevasse or ravine. Due to its shape the trajectory of the gradient descent might bounce around off the walls of the curvature drastically increasing training time. Another issue associated with SGD is the vanishing gradient problem (or its opposite -- the exploding gradient) which occurs at plateaus (cliffs). Having a gradient of zero means that no further improvements to the model can be made. In essence learning has stopped. 

To tackle these issues advanced optimizes use what is know as \textbf{momentum} and \textbf{adaptive learning rate}. The idea behind momentum is quite simple instead of switching update direction all the time some of the previous updates trajectories are maintained, hence the name momentum. This has the effect of dampening the oscillations in the pathological curvature scenario previously explained which of course leads to faster converging. In the plateau scenario momentum from the previous updates can carry the gradient trajectory to a different region where the gradient in not zero, effectively escaping the plateau.

Another problem that might occur when using vanilla SGD is having a constant learning rate for all parameters. This is problematic as having too big of a learning rate will lead to overshooting and too small -- to slow training. Additionally, some parameters might be closer to converging than other hence will need a smaller update. Adaptive learning rate tries to solve this problem by using different learning rate decay schedulers or even having a separate learning rate for each weight. Some of the popular decay schedulers include step decay, inverse decay and exponential decay.  

\subsection*{Question 1.5a}

The forget gate of the LSTM as the name suggests has the purpose of deciding how much of the previously retained information will be removed ("forgotten"). Similarly, the input gate decides how much of the new information will be added to the cell state. The input modulation gate is used to modulate the information the previously mentioned input gate will write to the cell state. Finally, the output gate determines how much of the information from the current hidden and cell states will go to the next hidden state.

Two types of non-linearity are used in the LSTM. The sigmoid activation is used, for example, in the input and forgetting gate as it is an easy indicator of how much information to get passed along -- 1 meaning all of the information and 0 meaning none of it. The tahn activation function is also used in the input modulation gate and when computing the next hidden state. The tahn activation is preferred in such cases over the sigmoid as its derivative is centered around zero.     

\subsection*{Question 1.5b}

The LSTM consists of 4 gates (input, input modulation, forget and output gate). Each gate contains two matrices (one is $d \times n$ the other one $n \times n$) plus a bias (size $n$). So each gate consists of $(d \times n) + (n \times n) + n = n \times (d \times n \times 1)$ parameters. Since there are 4 such gates the total goes up to $4n \times (d \times n \times 1)$

\subsection*{Question 1.6}

The same setup as described in \textbf{Question 1.3} was used to conduct an experiment on the LSTM accuracy. Again, using one hot encoded inputs and all other parameters at their defaults. The obtained results can be seen in \textit{Figure \ref{fig:lstm_acc}} 

Comparing the LSTM plot (\textit{Figure \ref{fig:lstm_acc}}) to the RNN one (\textit{Figure \ref{fig:rnn_acc}}) we can see that for up to $T = 12$ the LSTM manages to achieve 100\% accuracy and 0 loss for the specified 10000 steps, unlike the RNN which took longer to reach the same accuracy level for $T = 10$ and achieved only 80\% accuracy for $T = 12$ at a loss slightly higher than 0. Furthermore, the LSTM achieves 60\% accuracy for $T = 15$ at 2 loss, compared to the 40\% accuracy and 2.5 loss achieved by the RNN. Furthermore, the trends shown in \textit{Figure \ref{fig:rnn_acc}} seem much more saturated compared to the ones seen in \textit{Figure \ref{fig:lstm_acc}} meaning that with more training steps the LSTM would have achieved an even higher accuracy score for $T = 13$, $T = 14$ and $T = 15$. So to summarize, on average the LSTM is able to retain more information and converges faster than the RNN.

A summary plot comparing the maximum accuracy and minimum loss of the RNN and LSTM over the palindrome length can be found in \textit{Figure \ref{fig:lstm_rnn_summary}}. The figure shows that the LSTM on average retains more information at a slightly higher loss.  

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/LSTM.png}
	\caption{Accuracy and Loss of LSTM for different T values over time}
	\label{fig:lstm_acc}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/summary.png}
	\caption{Max accuracy and min loss of RNN and LSTM}
	\label{fig:lstm_rnn_summary}
\end{figure}

\subsection*{Question 1.7}

The gradients over time plot can be found in \textit{Figure \ref{fig:grads_time}}. Initialization of the weights proved to be important for the comparison of the two types of network. In order to obtain good results Xavier initialization was used on all weight matrices.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/grads_over_time.png}
	\caption{Gradients of RNN and LSTM over time for palindrome of length 10}
	\label{fig:grads_time}
\end{figure}

\section{Recurrent Nets as Generative Model}

\subsection*{Question 2.1a}

The source code for the two-layer LSTM can be found in \verb|model.py|. The training script can be found in \verb|train.py|. After training the two-layer LSTM for 100000 training steps for sequences of length $T = 30$ the network achieves an overall accuracy of 0.7 with a loss of around 1 (see \textit{Figure \ref{fig:lstm_30}}). Training was performed on \verb|book_EN_grimms_fairy_tails.txt|. All of the hyperparameters were kept at their defaults, apart from the number of training steps which was set to 100000. Looking at \textit{Figure \ref{fig:lstm_30}} the network converges much sooner (at around 25000 training steps) meaning the experiment could have been carried out for a smaller number of training steps.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/lstm_text_30.png}
	\caption{Accuracy and Loss for two-layer LSTM for $T = 30$}
	\label{fig:lstm_30}
\end{figure}

\subsection*{Question 2.1b}

In order to discuss the text being generated by the network at different stages of training for different sequence lengths two more experiments where conducted. The setup was the exact same as discussed in \textbf{Question 2.1a} with the only difference being that one trial used $T = 15$ and the other $T = 45$ as the sequence length of the text to be generated. The plots from the two experiments can be seen in \textit{Figure \ref{fig:lstm_15}} and \textit{Figure \ref{fig:lstm_45}} respectively.
	
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/lstm_text_15.png}
	\caption{Accuracy and Loss for two-layer LSTM for $T = 15$}
	\label{fig:lstm_15}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{./img/lstm_text_45.png}
	\caption{Accuracy and Loss for two-layer LSTM for $T = 45$}
	\label{fig:lstm_45}
\end{figure}

As to be expected within the first few training iterations all three networks generated text which consisted mostly of repeating words. For $T = 15$ -- ``\textit{k to to to to t}'', for $T = 30$ -- ``\textit{6 te the the the the the the t}'' and for $T = 45$ -- ``\textit{œthe wand the wand the wand the wand the wand}''. Even though this text is meaningless it is interesting to see that with very few training iterations the network already produces almost grammatically correct text as almost all of the words contained in the example are actual words in the English language.

At around 3000 training iterations we have the following examples, for $T = 15$ -- ``\textit{And the shall b}'', for $T = 30$ -- ``\textit{Do she was so that he could no}'' and for $T = 45$ -- ``\textit{ke the second she was all the window, and sai}''. Here we see that the formation of words is almost perfect with the repeating of words patter we saw in the previous example completely gone. The generated text also seems to be quite close to being semantically correct.

At around 10000 training iterations we have the following examples, for $T = 15$ -- ``\textit{™ said the fish}'', for $T = 30$ -- ``\textit{When the fox said to him, âa˜I}'' and for $T = 45$ -- ``\textit{0 The musicians were so swim and said: âa˜I w}''. Here we can see an interesting patter emerging, the text is almost semantically correct, however, the network is struggling to understand the concept of quotations.

At around 20000 training iterations we have the following examples, for $T = 15$ -- ``\textit{the castle, and}'', for $T = 30$ -- ``\textit{Gretel sat at the spinning-whe}'' and for $T = 45$ -- ``\textit{X AND THE SEVEN LITTLE KIDS}''. In the first example we can see that the network is now able to generate also correct punctuation. The second example we can easily see how it would be a complete sentence about Gretel sitting at the spinning-wheel. And the last example is quite interesting as the network seems to understand how to generate story titles as the text is in all capital letters and has the general structure of a title. Although, the use of the name ``X'' is far from being perfect.  

Finally, some examples at the end of the 100000 training steps, for $T = 15$ -- ``\textit{My soldier went}'', for $T = 30$ -- ``\textit{the wolf had a wonderful thin}'' and for $T = 45$ -- ``\textit{When the two white doves could not be afraide}''. The text generated here is semantically and grammatically correct, apart from the misspelling of the word \textit{afraid} in the last example.

Overall, for all three sequence lengths the text generated is coherent but it does seem to take longer to become more coherent for longer sequence lengths.

\subsection*{Question 2.1c}

Instead of applying an argmax on the final softmax function now we sample according to the probability distribution given by the softmax. The use of the temperature parameter $\tau$ controls how random the sampling is. The higher the temperature the more random the sampling. Another three experiments were conducted having the exact same setup as explained in \textbf{Question 2.1a} for sequence length $T = 30$ and the three temperature values -- $0.5$, $1$ and $2$. During the end of the training cycle we have the following examples. For $\tau = 0.5$ -- ``\textit{THE TWELVE HE BIND OR NA}'', ``\textit{ve me, I can have you thinking}'', ``\textit{uch a stone and that he had go}''. For $\tau = 1.0$ -- ``\textit{fully as she had done talk, ca}'', ``\textit{Catherine steal-6, I will go h}'', ``\textit{commands gull. Fox were to see}''. For $\tau = 2.0$ -- ``\textit{There Sniviked pain, befor}'', ``\textit{pond moyed ferrain, whenchinio}'', ``\textit{was, and in ambtixetone; for I}''.

As previously mentioned the higher the temperature the more random the sampling which is also reflected in the examples given above. When compared to the greedy sampling, these examples are far less coherent and contain far more grammatical/semantical errors.

\section{Graph Neural Networks}

\subsection*{Question 3.1a}

The propagation rule for a layer in a Graph Convolutional Network is the following:

\begin{equation}
	H^{(l + 1)} = \sigma(\hat{A}H^{(l)}W^{(l)})
	\label{eq:5}
\end{equation}

The propagation rule shown in \eqref{eq:5} is quite similar to the update rule in any feed-forward network as the inputs at time step $l$ are multiplied by the weight matrix $W$ and passed through a non-linear activation function (in this case a sigmoid) to obtain the outputs. The difference comes from the fact that before the inputs $H^{(l)}$ are propagated through the network they are first multiplied by another matrix $\hat{A}$. This matrix is called the adjacency matrix which is nothing more than a matrix representation of the connections between nodes in the graph. Since $\hat{A}$ represents connections between \textbf{all} nodes in the network it also reflects \textbf{self connectivity} i.e. the state of a node can be propagated forward in time. The GCN layer can be seen as message passing over the graph as in order to calculate the new state of a node its neighboring nodes and the nodes previous state are used. This is a result of multiplying the adjacency matrix with the inputs a time step resulting in the sum of embeddings.

\subsection*{Question 3.1b}

Currently, the adjacency matrix is described as follows:

\begin{equation}
	\hat{A} = A + I_N
\end{equation} 

The $I_N$ term here is introduced to reflect the self connections in the network. However, by doing so a limiting assumption is being introduced. Namely, an equal importance is being placed on self connections and edges to neighboring nodes. As mentioned in the original paper \cite{kipf2016semi} a trade-off parameter can be introduced in the following way:
\begin{equation}
	\hat{A} = A + \lambda I_N
\end{equation}

The parameter $\lambda$ can be learned through gradient descent.

\subsection*{Question 3.2a}

The adjacency matrix is given below:
\begin{equation}
	\hat{A} = \begin{pmatrix}
	1 & 1 & 0 & 0 & 1 & 1 \\
	1 & 1 & 1 & 1 & 0 & 0 \\
	0 & 1 & 1 & 1 & 0 & 0 \\
	0 & 1 & 1 & 1 & 0 & 1 \\
	1 & 0 & 0 & 0 & 1 & 1 \\
	1 & 0 & 0 & 1 & 1 & 1 \\
	\end{pmatrix}
\end{equation}

\subsection*{Question 3.2b}

Updating node D will forward the information from node C. Then updating node F would further propagate the information from node C. And finally updating one more time would forward the information to node E. The total number of updates is 3.

\subsection*{Question 3.3}

\begin{itemize}
	\item Text classification -- \cite{henaff2015deep}, \cite{atwood2016diffusion}, \cite{hamilton2017inductive}
	
	\item Neural machine translation -- \cite{beck2018graph}
	
	\item Image classification -- \cite{garcia2017few}, \cite{wang2018zero}
	
	\item Molecular fingerprints -- \cite{kearnes2016molecular}
\end{itemize}

\subsection*{Question 3.4a}

The main difference between the types of data that the two networks can be applied to has to do with order. RNNs are often applied to ordered data, one such example is the text generation task which was discussed in the previous two sections. GNNs are more often applied to unordered data where graph and tree structures exist. In \textbf{Question 3.4a} a few such cases were mentioned, like machine translation, in fact GNNs are often used in the field of Natural Language Processing.

Furthermore, RNNs and their LSTM variant are able to store information for further back in time. This is important for data and tasks which require such retention of information over longer time frames. GNNs on the other hand can also be viewed as a time series but here the passing of information happens in a local area. That is states are dependent on their neighbors meaning GNNs are more suitable for data which is defined but such local connections.

\subsection*{Question 3.4b}

A task which would benefit from using a combination of GNNs and RNNs would have to fulfill both of the requirements in the section above. That is mainly time and spacial dependencies. One example that comes to mind which could be applicable in this case is what is known as a Tree LSTM. The underlying structure of such a task is a tree (which would be suitable for using a GNN on) and the retention information over multiple time frames is needed (the use of and RNN in this case LSTM). It would be interesting to see if the two networks manage to compliment each other in such a use case.        

\bibliographystyle{ieeetr}
\bibliography{ref} 

\end{document}
