import argparse

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from torchvision.utils import make_grid

from datasets.bmnist import bmnist

import numpy as np
from scipy.stats import norm


class Encoder(nn.Module):

    def __init__(self, hidden_dim=500, z_dim=20, x_dim=784):
        super().__init__()

        # mean: from original dim to latent dim
        self.mean = nn.Sequential(
                nn.Linear(x_dim, hidden_dim),
                nn.ReLU(), # TODO: experiment with other activations if time left
                nn.Linear(hidden_dim, z_dim)
            )

        # std: from original dim to latent dim
        self.std = nn.Sequential(
                nn.Linear(x_dim, hidden_dim),
                nn.ReLU(), # TODO: experiment with other activations if time left
                nn.Linear(hidden_dim, z_dim),
                nn.ReLU() # this one is important as std cannot be negative
            )

    def forward(self, input):
        """
        Perform forward pass of encoder.

        Returns mean and std with shape [batch_size, z_dim]. Make sure
        that any constraints are enforced.
        """

        mean = self.mean(input)
        std = self.std(input)

        return mean, std


class Decoder(nn.Module):

    def __init__(self, hidden_dim=500, z_dim=20, x_dim=784):
        super().__init__()

        # from latent dim to original dim
        # final activation is between 0 and 1
        self.decoder = nn.Sequential(
                nn.Linear(z_dim, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, x_dim),
                nn.Sigmoid() # not ReLu
            )

    def forward(self, input):
        """
        Perform forward pass of encoder.

        Returns mean with shape [batch_size, 784].
        """
        mean = self.decoder(input)

        return mean


class VAE(nn.Module):

    def __init__(self, hidden_dim=500, z_dim=20, x_dim=784, device='cuda:0'):
        super().__init__()

        self.z_dim = z_dim
        self.x_dim = x_dim
        self.device = device
        self.encoder = Encoder(hidden_dim, z_dim, x_dim).to(device)
        self.decoder = Decoder(hidden_dim, z_dim, x_dim).to(device)

    def forward(self, input):
        """
        Given input, perform an encoding and decoding step and return the
        negative average elbo for the given batch.
        """

        # reshape input
        input = input.view(-1, self.x_dim).to(self.device)

        # get mean and std from encoder
        mean, std = self.encoder(input)

        # z = M + SD * epsilon
        z = mean + std * torch.randn((1, self.z_dim)).to(self.device)

        # get output from decoder
        output = self.decoder(z)

        # not to break log
        eps = 1e-12

        # reconstruction
        l_rec = -(input * (output + eps).log() + (1 - input) * (1 - output + eps).log()).sum(dim=1)

        # regularization
        l_reg = 0.5 * ( - 2 * (std + eps).log() + (std**2 + mean**2 - 1)).sum(dim=1)

        # return mean elbo score
        return (l_rec + l_reg).mean(dim=0)

    @torch.no_grad()
    def sample(self, n_samples):
        """
        Sample n_samples from the model. Return both the sampled images
        (from bernoulli) and the means for these bernoullis (as these are
        used to plot the data manifold).
        """

        # sample z's from latent space 
        z = torch.randn((n_samples, self.z_dim)).to(self.device)

        # get samples
        samples = self.decoder(z)

        # pass through bernoulli
        samples = torch.bernoulli(samples)

        return samples, samples.mean(dim=0)


def epoch_iter(model, data, optimizer):
    """
    Perform a single epoch for either the training or validation.
    use model.training to determine if in 'training mode' or not.

    Returns the average elbo for the complete epoch.
    """
    average_epoch_elbo = 0
    
    for batch in data:

        # get elbo for current batch
        batch_elbo = model.forward(batch)

        if model.training:

            # zero out gradients
            model.zero_grad()

            # backprop
            batch_elbo.backward()

            # step using optimizer
            optimizer.step()

        # average
        average_epoch_elbo += (batch_elbo.item()  / len(data))

    return average_epoch_elbo


def run_epoch(model, data, optimizer):
    """
    Run a train and validation epoch and return average elbo for each.
    """
    traindata, valdata = data

    model.train()
    train_elbo = epoch_iter(model, traindata, optimizer)

    model.eval()
    val_elbo = epoch_iter(model, valdata, optimizer)

    return train_elbo, val_elbo


def save_elbo_plot(train_curve, val_curve, filename):
    plt.figure(figsize=(12, 6))
    plt.plot(train_curve, label='train elbo')
    plt.plot(val_curve, label='validation elbo')
    plt.legend()
    plt.xlabel('epochs')
    plt.ylabel('ELBO')
    plt.tight_layout()
    plt.savefig(filename)


# as to not update any of the weights
@torch.no_grad()
def sample_images(model, epoch_num, n_samples=100):
    images, _ = model.sample(n_samples)

    # get row size
    row = int(np.sqrt(n_samples))

    # get image size default is 28 
    im_size = int(np.sqrt(model.x_dim))

    # go back to 1 x 28 x 28 from flat vector
    # the one color channel is importnat for make_grid
    images = images.view(-1, 1, im_size, im_size)

    # do image grid and swap axes
    # ax1 x ax2 x ax3 becomes ax2 x ax3 x ax1
    sample_ims = make_grid(images, nrow = row).permute(1, 2, 0).cpu().numpy()

    plt.imsave("sample_{}.png".format(epoch_num), sample_ims)

# as to not update any of the weights
@torch.no_grad()
def manifold_image(model, n_examples=400):

    # get number of rows in manifold
    n_rows = int(np.sqrt(n_examples))

    # generate array of values to perform inverse CDF on
    # only one is needed as passing the same values would reslt
    # in the same array
    values = torch.linspace(0.05, 0.95, n_rows)

    # generate all samples
    z = []
    for dim1 in values:
        for dim2 in values:
            # make tensor with ppf values and add to array
            z.append(torch.tensor([norm.ppf(dim1), norm.ppf(dim2)]))

    # stack all vectors in one tensor
    z = torch.stack(z).to(model.device)
    
    # Pass samples through decoder
    # and reshape for make_grid
    output = model.decoder(z).view(-1, 1, int(np.sqrt(model.x_dim)), int(np.sqrt(model.x_dim)))

    # pass to make grid and swap axes again
    manifold = make_grid(output, nrow = n_rows).permute(1, 2, 0).cpu().numpy()

    # save to file
    plt.imsave("manifold.png", manifold)


def main():
    data = bmnist()[:2]  # ignore test split
    model = VAE(z_dim=ARGS.zdim, device=ARGS.device, x_dim=ARGS.xdim)
    optimizer = torch.optim.Adam(model.parameters())

    train_curve, val_curve = [], []
    for epoch in range(ARGS.epochs):
        elbos = run_epoch(model, data, optimizer)
        train_elbo, val_elbo = elbos
        train_curve.append(train_elbo)
        val_curve.append(val_elbo)
        print("[Epoch {}] train elbo: {} val_elbo: {}".format(epoch, train_elbo, val_elbo))

        # --------------------------------------------------------------------
        #  Add functionality to plot samples from model during training.
        #  You can use the make_grid functioanlity that is already imported.
        # --------------------------------------------------------------------
        if epoch == 0 or epoch == ARGS.epochs / 2 or epoch == ARGS.epochs - 1:
            sample_images(model, epoch, ARGS.nsamples) 

    # --------------------------------------------------------------------
    #  Add functionality to plot plot the learned data manifold after
    #  if required (i.e., if zdim == 2). You can use the make_grid
    #  functionality that is already imported.
    # --------------------------------------------------------------------
    if ARGS.zdim == 2:
        manifold_image(model, ARGS.manifold_n_samples)

    save_elbo_plot(train_curve, val_curve, 'elbo.png')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=40, type=int,
                        help='max number of epochs')
    parser.add_argument('--zdim', default=20, type=int,
                        help='dimensionality of latent space')
    parser.add_argument('--xdim', type=int, default=784, 
                        help="Original vector space")
    parser.add_argument('--nsamples', type=int, default=100, 
                        help="Number of images to sample. Must be of the form n = x^2")
    parser.add_argument('--manifold_n_samples', type=int, default=400, 
                        help="Number of examples for manifold in N x N grid. Should sqrt-able")
    parser.add_argument('--device', type=str, default="cuda:0", 
                        help="Training device 'cpu' or 'cuda:0'")

    ARGS = parser.parse_args()

    main()
