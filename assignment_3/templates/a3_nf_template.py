import argparse

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np
from datasets.mnist import mnist
import os
from torchvision.utils import make_grid

import numpy as np

def log_prior(x):
    """
    Compute the elementwise log probability of a standard Gaussian, i.e.
    N(x | mu=0, sigma=1).
    """
    logp = (-np.log(np.sqrt(2 * np.pi)) - 0.5 * x**2).sum(dim=1)

    return logp


def sample_prior(size, device="cuda:0"):
    """
    Sample from a standard Gaussian.
    """
    sample = torch.randn(size).to(device)

    return sample


def get_mask():
    mask = np.zeros((28, 28), dtype='float32')
    for i in range(28):
        for j in range(28):
            if (i + j) % 2 == 0:
                mask[i, j] = 1

    mask = mask.reshape(1, 28*28)
    mask = torch.from_numpy(mask)

    return mask


class Coupling(torch.nn.Module):
    def __init__(self, c_in, mask, n_hidden=1024):
        super().__init__()
        self.n_hidden = n_hidden

        # Assigns mask to self.mask and creates reference for pytorch.
        self.register_buffer('mask', mask)

        # Create shared architecture to generate both the translation and
        # scale variables.
        # Suggestion: Linear ReLU Linear ReLU Linear.
        self.nn = torch.nn.Sequential(
                nn.Linear(c_in, n_hidden),
                nn.ReLU(),
                nn.Linear(n_hidden, n_hidden),
                nn.ReLU(),
                nn.Linear(n_hidden, 2 * c_in) # to be able to extract translation and scale
            )

        # The nn should be initialized such that the weights of the last layer
        # is zero, so that its initial transform is identity.
        self.nn[-1].weight.data.zero_()
        self.nn[-1].bias.data.zero_()

    def forward(self, z, ldj, reverse=False):
        # Implement the forward and inverse for an affine coupling layer. Split
        # the input using the mask in self.mask. Transform one part with
        # Make sure to account for the log Jacobian determinant (ldj).
        # For reference, check: Density estimation using RealNVP.

        # NOTE: For stability, it is advised to model the scale via:
        # log_scale = tanh(h), where h is the scale-output
        # from the NN.

        # apply mask to z
        z_mask = self.mask * z

        # get translation and scale output from NN
        scale, trans = self.nn(z_mask).chunk(2, dim=1)

        # pass through tanh
        log_scale = torch.tanh(scale)

        if not reverse:
            z = z_mask + (1 - self.mask) * (z * log_scale.exp() + trans)

            ldj += ((1 - self.mask) * log_scale).sum(dim=1)
        else:
            z = z_mask + (1 - self.mask) * ((z - trans) * (-log_scale).exp())

            ldj.zero_()
        return z, ldj


class Flow(nn.Module):
    def __init__(self, shape, n_flows=4):
        super().__init__()
        channels, = shape

        mask = get_mask()

        self.layers = torch.nn.ModuleList()

        for i in range(n_flows):
            self.layers.append(Coupling(c_in=channels, mask=mask))
            self.layers.append(Coupling(c_in=channels, mask=1-mask))

        self.z_shape = (channels,)

    def forward(self, z, logdet, reverse=False):
        if not reverse:
            for layer in self.layers:
                z, logdet = layer(z, logdet)
        else:
            for layer in reversed(self.layers):
                z, logdet = layer(z, logdet, reverse=True)

        return z, logdet


class Model(nn.Module):
    def __init__(self, shape, device="cuda:0"):
        super().__init__()
        self.flow = Flow(shape)
        self.device = device

    def dequantize(self, z):
        return z + torch.rand_like(z)

    def logit_normalize(self, z, logdet, reverse=False):
        """
        Inverse sigmoid normalization.
        """
        alpha = 1e-5

        if not reverse:
            # Divide by 256 and update ldj.
            z = z / 256.
            logdet -= np.log(256) * np.prod(z.size()[1:])

            # Logit normalize
            z = z*(1-alpha) + alpha*0.5
            logdet += torch.sum(-torch.log(z) - torch.log(1-z), dim=1)
            z = torch.log(z) - torch.log(1-z)

        else:
            # Inverse normalize
            z = torch.sigmoid(z)
            logdet += torch.sum(torch.log(z) + torch.log(1-z), dim=1)
            z = (z - alpha*0.5)/(1 - alpha)

            # Multiply by 256.
            logdet += np.log(256) * np.prod(z.size()[1:])
            z = z * 256.

        return z, logdet

    def forward(self, input):
        """
        Given input, encode the input to z space. Also keep track of ldj.
        """
        z = input
        ldj = torch.zeros(z.size(0), device=z.device)

        z = self.dequantize(z)
        z, ldj = self.logit_normalize(z, ldj)

        z, ldj = self.flow(z, ldj)

        # Compute log_pz and log_px per example

        log_pz = log_prior(z)

        log_px = log_pz + ldj

        return log_px

    def sample(self, n_samples):
        """
        Sample n_samples from the model. Sample from prior and create ldj.
        Then invert the flow and invert the logit_normalize.
        """
        z = sample_prior((n_samples,) + self.flow.z_shape, self.device)
        ldj = torch.zeros(z.size(0), device=z.device)

        # invert flow
        z, ldj = self.flow(z, ldj, reverse=True)

        # invert logit_normalize
        z, ldj = self.logit_normalize(z, ldj, reverse=True)

        return z


def epoch_iter(model, data, optimizer, device="cuda:0", print_batch_info=False):
    """
    Perform a single epoch for either the training or validation.
    use model.training to determine if in 'training mode' or not.

    Returns the average bpd ("bits per dimension" which is the negative
    log_2 likelihood per dimension) averaged over the complete epoch.
    """

    avg_bpd = 0

    for i, (batch, _) in enumerate(data):

        # printing stuff to show progress within batch
        if print_batch_info:
            count = len(data)
            size = 70
            x = int(size*i/count)
            print("%s[%s%s%s] %i/%i" % ("Batch: ", "#"*(x-1), ">", "."*(size-x-1), i, count), end="\r")

        loss = -model(batch.reshape(batch.shape[0], -1).to(device)).mean()

        if model.training:

            # zero grad
            model.zero_grad()

            # backprop
            loss.backward()

            # take step
            optimizer.step()

        avg_bpd += loss.item() / (len(data) * 784 * np.log(2))

    # other printing stuff
    if print_batch_info:
        count = len(data)
        size = 70
        x = int(size*i/count)
        print("%s[%s%s%s] %i/%i" % ("Batch: ", "#"*(x-1), ">", "."*(size-x-1), i, count))

    return avg_bpd 


def run_epoch(model, data, optimizer, device="cuda:0", print_batch_info=False):
    """
    Run a train and validation epoch and return average bpd for each.
    """
    traindata, valdata = data

    model.train()
    train_bpd = epoch_iter(model, traindata, optimizer, device, print_batch_info)

    model.eval()
    val_bpd = epoch_iter(model, valdata, optimizer, device, print_batch_info)

    return train_bpd, val_bpd


def save_bpd_plot(train_curve, val_curve, filename):
    plt.figure(figsize=(12, 6))
    plt.plot(train_curve, label='train bpd')
    plt.plot(val_curve, label='validation bpd')
    plt.legend()
    plt.xlabel('epochs')
    plt.ylabel('bpd')
    plt.tight_layout()
    plt.savefig(filename)


def main():
    data = mnist()[:2]  # ignore test split

    model = Model(shape=[784]).to(ARGS.device)

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)

    os.makedirs('images_nfs', exist_ok=True)

    train_curve, val_curve = [], []
    for epoch in range(ARGS.epochs):
        bpds = run_epoch(model, data, optimizer, ARGS.device, ARGS.print_batch_info)
        train_bpd, val_bpd = bpds
        train_curve.append(train_bpd)
        val_curve.append(val_bpd)
        print("[Epoch %d] train bpd: %.2f val_bpd: %.2f" % (epoch, train_bpd, val_bpd))

        # --------------------------------------------------------------------
        #  Add functionality to plot samples from model during training.
        #  You can use the make_grid functionality that is already imported.
        #  Save grid to images_nfs/
        # --------------------------------------------------------------------

        # get samples and reshape to 28 x 28
        samples = model.sample(ARGS.n_samples).reshape(ARGS.n_samples, 1, 28, 28)

        # get n rows
        rows = int(np.sqrt(ARGS.n_samples))

        # make grid, normalize flag is important
        images = make_grid(samples, nrow = rows, normalize=True).permute(1,2,0).cpu().detach().numpy()

        # save to system
        plt.imsave("images_nfs/e_{}".format(epoch), images)


    save_bpd_plot(train_curve, val_curve, "nfs_bpd_{}.png".format(ARGS.epochs))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=40, type=int,
                        help='max number of epochs')
    parser.add_argument('--device', type=str, default="cuda:0", 
                        help="Training device 'cpu' or 'cuda:0'")
    parser.add_argument('--n_samples', type=int, default=100, 
                        help="Number of samples to generate. Has to be sqrt-able")
    parser.add_argument('--print_batch_info', type=bool, default=False, 
                        help="Prints loading bar")
    ARGS = parser.parse_args()

    main()
