import argparse
import os

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torchvision import datasets

import numpy as np


class Generator(nn.Module):
    def __init__(self, latent_dim, leaky_param):
        super(Generator, self).__init__()

        # Construct generator. You are free to experiment with your model,
        # but the following is a good start:
        #   Linear args.latent_dim -> 128
        #   LeakyReLU(0.2)
        #   Linear 128 -> 256
        #   Bnorm
        #   LeakyReLU(0.2)
        #   Linear 256 -> 512
        #   Bnorm
        #   LeakyReLU(0.2)
        #   Linear 512 -> 1024
        #   Bnorm
        #   LeakyReLU(0.2)
        #   Linear 1024 -> 768
        #   Output non-linearity

        self.layers = nn.Sequential(
                nn.Linear(latent_dim, 128),
                nn.LeakyReLU(leaky_param),
                nn.Linear(128, 256),
                nn.BatchNorm1d(256),
                nn.LeakyReLU(leaky_param),
                nn.Linear(256, 512),
                nn.BatchNorm1d(512),
                nn.LeakyReLU(leaky_param),
                nn.Linear(512, 1024),
                nn.BatchNorm1d(1024),
                nn.LeakyReLU(leaky_param),
                nn.Linear(1024, 784),
                nn.Tanh()
            )

    def forward(self, z):
        
        return self.layers(z)


class Discriminator(nn.Module):
    def __init__(self, leaky_param):
        super(Discriminator, self).__init__()

        # Construct distriminator. You are free to experiment with your model,
        # but the following is a good start:
        #   Linear 784 -> 512
        #   LeakyReLU(0.2)
        #   Linear 512 -> 256
        #   LeakyReLU(0.2)
        #   Linear 256 -> 1
        #   Output non-linearity

        self.layers = nn.Sequential(
                nn.Linear(784, 512),
                nn.LeakyReLU(leaky_param),
                nn.Linear(512, 256),
                nn.LeakyReLU(leaky_param),
                nn.Linear(256, 1),
                nn.Sigmoid()
            )

    def forward(self, img):
        
        return self.layers(img)


def train(dataloader, discriminator, generator, optimizer_G, optimizer_D):

    # get device
    device = torch.device(args.device)

    # send models to device
    generator = generator.to(device)
    discriminator = discriminator.to(device)

    for epoch in range(args.n_epochs):

        epoch_gen_loss, epoch_disc_loss = [], []

        for i, (imgs, _) in enumerate(dataloader):
            
            batch_size = imgs.shape[0]

            imgs.cuda()

            # Train Generator
            # ---------------

            # get random input vector for each batch entry
            z = torch.randn(batch_size, args.latent_dim).to(device)

            # get output
            dis_output = discriminator(generator(z))

            # compute loss
            gen_loss = -torch.log(dis_output).mean()
            epoch_gen_loss.append(gen_loss.item())

            # zero grad
            optimizer_G.zero_grad()

            # propagate loss backwards
            gen_loss.backward()

            # optimizer update
            optimizer_G.step()

            # Train Discriminator
            # -------------------

            # get discriminator classifications
            # compute again as buffers are full
            disc_classifications = discriminator(generator(z))

            # reshape data
            imgs = imgs.view(batch_size, 784).to(device)

            # get real calssifications on real data
            real_classifications = discriminator(imgs)

            # compute loss
            dis_loss = -(torch.log(real_classifications) + torch.log(1 - disc_classifications)).mean()
            epoch_disc_loss.append(dis_loss.item())

            # zero grad
            optimizer_D.zero_grad()

            # propagate los backwards
            dis_loss.backward()

            # optimizer update
            optimizer_D.step()

            # Save Images
            # -----------
            batches_done = epoch * len(dataloader) + i
            if batches_done % args.save_interval == 0:
                # You can use the function save_image(Tensor (shape Bx1x28x28),
                # filename, number of rows, normalize) to save the generated
                # images, e.g.:
                sample_imgs = generator(z[:25]).view(-1, 1, 28, 28)
                save_image(sample_imgs,
                           'images/e{}_b{}.png'.format(epoch, batches_done),
                           nrow=5, normalize=True)
                pass

        print("Epoch: %d | Gen loss: %.4f | Dis loss: %.4f" 
            % (epoch, 
                np.sum(epoch_gen_loss) / len(epoch_gen_loss), 
                np.sum(epoch_disc_loss) / len(epoch_disc_loss)))


def main():
    # Create output image directory
    os.makedirs('images', exist_ok=True)

    # load data
    dataloader = torch.utils.data.DataLoader(
        datasets.MNIST('./data/mnist', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.5, ),
                                                (0.5, ))])),
        batch_size=args.batch_size, shuffle=True)

    # Initialize models and optimizers
    generator = Generator(args.latent_dim, args.leaky_param)
    discriminator = Discriminator(args.leaky_param)
    optimizer_G = torch.optim.Adam(generator.parameters(), lr=args.lr)
    optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=args.lr)

    # Start training
    train(dataloader, discriminator, generator, optimizer_G, optimizer_D)

    # You can save your generator here to re-use it to generate images for your
    # report, e.g.:
    torch.save(generator.state_dict(), "mnist_generator.pt")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_epochs', type=int, default=200,
                        help='number of epochs')
    parser.add_argument('--batch_size', type=int, default=64,
                        help='batch size')
    parser.add_argument('--lr', type=float, default=0.0002,
                        help='learning rate')
    parser.add_argument('--latent_dim', type=int, default=100,
                        help='dimensionality of the latent space')
    parser.add_argument('--save_interval', type=int, default=500,
                        help='save every SAVE_INTERVAL iterations')
    parser.add_argument('--leaky_param', type=float, default=0.2,
                        help='Leaky parameter for LeakyReLU activation')
    parser.add_argument('--device', type=str, default="cuda:0", 
                        help="Training device 'cpu' or 'cuda:0'")
    args = parser.parse_args()

    main()
