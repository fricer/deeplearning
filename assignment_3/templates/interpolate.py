import numpy as np
import torch
from torchvision.utils import make_grid
from torchvision.utils import save_image
from a3_gan_template import Generator
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

n_images = 25
latent_dim = 100

z = torch.randn(n_images, latent_dim)

model = Generator(latent_dim, 0.2)
model.load_state_dict(torch.load("gan_mnist_generator.pt"))

samples = model(z).view(-1, 1, 28, 28)

# save_image(samples, 'test.png',nrow=5, normalize=True)

# print(samples[1].shape)
# # save to system
plt.imshow(samples.detach().numpy()[0][0], cmap='Greys')
plt.show()

plt.imshow(samples.detach().numpy()[1][0], cmap='Greys')
plt.show()

images = samples.view(-1, 784).detach().numpy()
im1 = images[0]
im2 = images[1]

print(im1.shape)
print(im2.shape)

test = interp1d([1,9], np.vstack([im1, im2]), axis=0)

output = test(np.arange(1,10))

int_ims = torch.from_numpy(output).view(-1, 1, 28, 28)

save_image(int_ims, 'test-1.png', nrow=9, normalize=True)

